<?php if(!defined('BASEPATH')) exit("No direct script access allowed");

/**
 * 发送手机信息
 * @author y
 *
 */
class Sms{


	var $content = "";
	var $phone = 0;

	public function __construct()
	{
		$CI =& get_instance();


		$this->phone = $CI->input->post('phone');
		$this->content = $CI->input->post('content');
		// 如果没有POST数据，就试图从GET得到
		if($this->phone == FALSE || $this->content == FALSE){
			$this->phone = $CI->input->get('phone');
			$this->content = $CI->input->get('content');
			//对信息内容进行URL解码
			$this->content = urldecode($this->content);
		}

	}


	/**
	 * 添加到队列中
	 */
	private function EnterQueue() {
		//对信息内容进行URL编码
		$this->content = urlencode($this->content);
		//如果在SAE上就使用SAE的任务队列
		$CI =& get_instance();
		if($CI->config->item('sae')){
			$queue = new SaeTaskQueue('sms');
			$queue = new SaeTaskQueue('sms');
			$queue->addTask("http://".$_SERVER['HTTP_HOST']."/sms/send",
								"phone={$this->phone}&content={$this->content}");
			$ret = $queue->push();
		}else{
			//TODO 本地模拟队列
			//			$this->send();
			$url = "http://localhost/fanfoucanyin/sms/send?phone={$this->phone}&content={$this->content}";
			//echo $url;
			// TODO 判断信息是否成功发出/送达
			$content = file_get_contents($url);

			//sleep(2);
		}
		return TRUE;
	}


	/**
	 * 调用PHPFetion，发送信息
	 */
	public function send() {
		if($this->phone !== FALSE && $this->content !== FALSE){
			$CI =& get_instance();

			$params = array(
					'mobile'=>$CI->config->item('fetion_phone'),
					'password'=>$CI->config->item('fetion_pass'),
			);
			$CI->load->library('PhpFetion',$params);

			$ret = $CI->phpfetion->send($this->phone, $this->content);
			
			/*
			$sms = apibus::init( "sms"); //创建短信服务对象
			$obj = $sms->send($this->phone, $this->content , "UTF-8");

			//错误输出 Tips: 亲，如果调用失败是不收费的 
			if ( $sms->isError( $obj ) ) {
			print_r( $obj->ApiBusError->errcode );
			print_r( $obj->ApiBusError->errdesc );
			}
			*/
			return $ret;
		}
		return FALSE;
	}


	/**
	 * 根据订单信息，得到短信的号码和内容
	 */
	public function get_sms_content($order_id, $r_id, $price) {
		$CI =& get_instance();

		$CI->load->model('order_model', '', TRUE);

		$info = $CI->order_model->get_sms_info($order_id, $r_id);


		$this->phone = $info['r_phone'];

		$this->content = "订单：{$info['customer']['phone']}\n地址：{$info['customer']['address']} {$info['customer']['building']} {$info['customer']['room']}\n菜品：\n";
		$temp_str = "";
		foreach($info['dish'] as $i => $v){
			$temp_str .= "{$i}:{$v['name']}……{$v['num']}份\n";
		}
		$this->content .= $temp_str;

		$this->content .= "---------\n金额总计：{$price}元\n";

	}



	/**
	 * 根据用户提交的订单，向每个定餐的商家都发送相应的订单信息
	 */
	public function order_sms($data) {
		foreach($data as $key => $v){
			//var_dump($v);
			$this->get_sms_content($v['order_id'], $v['r_id'], $v['price']);
			$this->EnterQueue();
		}
	}


}