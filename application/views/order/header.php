<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 tdansitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Cache-Controll" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<title><?=$title?></title>
	<!-- css -->
	<!-- 
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/"/>
	-->
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/common/reset.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/common/main.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/common/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/common/jsorder.css"/>


	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/jquery.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/jquery-ui.js"></script>
	<!-- js
	<script type='text/javascript' src="<?=STATIC_ROOT?>/js/"></script>
	 -->
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/jsorder.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/autoscroll.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/simpletip.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/my.js"></script>
	<script type='text/javascript'>
$(function(){
		$("#sn").simpletip({
			content:'订单号用于查询订单、更改订单、取消订单及送餐时确认您的身份，请注意保存',
			fixed:false
		});
});
	</script>
	<style>
	.tooltip{position: absolute;width: 300px;}
	</style>

	<!-- Google Analytics -->
	<script type='text/javascript'>
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-29875006-1']);
		  _gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	</script>

</head>

<body>

<div id="all">


