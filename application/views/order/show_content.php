<div id="show_order">

<table class="ui-corner-all table">

<tr class="table_header">
	<th></th>
	<th style="text-align:left;">订单信息</th>
</tr>

<tr>
	<td>订单号</td>
	<td id="sn"><?=$sn_show[0]?>&nbsp;<?=$sn_show[1]?>&nbsp;<?=$sn_show[2]?>&nbsp;<?=$sn_show[3]?></td>
</tr>
<tr class="tableOdd">
	<td>订餐时间</td>
	<td><?=$order_info['date']?>&nbsp;<?=$order_info['time']?></td>
</tr>
<tr>
	<td>送餐地址</td>
	<td><?=$order_info['address']?></td>
</tr>
<tr class="tableOdd">
	<td>送餐楼宇</td>
	<td><?=$order_info['building']?></td>
</tr>
<tr>
	<td>送餐房间</td>
	<td><?=$order_info['room']?></td>
</tr>
<tr class="tableOdd">
	<td>联系电话</td>
	<td><?=$order_info['phone']?></td>
</tr>

</table>
	


<table class="ui-corner-all table">
<tr class="table_header" >
	<th>菜品名</th>
	<th>餐厅名</th>
	<th>单价</th>
	<th>数量</th>
	<th>小计</th>
</tr>
<?php 
$i = 0;
foreach($dish as $key => $r):?>

<?php
$i++;
if($i % 2 != 0){?>
<tr>
<?php }else {?>
<tr class="tableOdd">
<?php }?>
	<td><?=$r['d_name']?></td>
	<td><?=$r['r_name']?></td>
	<td style="text-align:right;padding-right:15px"><?php printf("￥%.1f", $r['d_price'])?></td>
	<td><?=$r['d_num']?></td>
	<td style="text-align:right;padding-right:15px"><?php printf("￥%.1f", $r['d_price']*$r['d_num']); ?></td>
</tr>

<?php endforeach ?>
<!-- 
<tfoot>
<?php
$i++;
if($i % 2 != 0){?>
<tr>
<?php }else {?>
<tr class="tableOdd">
<?php }?>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td style="width:120px">
		<span>配送费:</span>
		<?php foreach($delivery as $v):?>
		<span><?=$v['r_name']?>:</span>
		<span><?=$v['price']?>元 &nbsp;</span>
		<?php endforeach;?>
<p style="width:120px">总计需支付￥<strong><?php printf("%.1f",$total_price)?></strong>元</p>
		</td>
	</tr>
</tfoot>
-->
</table>
<div id = "foot" >
<p style="margin-left:60px;font-size:10px;">
	<span>配送费:</span>
<?php foreach($delivery as $v):?>
	<span><?=$v['r_name']?>:</span>
	<span><?=$v['price']?>元 &nbsp;</span>
<?php endforeach;?>
</p>
<p id = "total" style="float:right;">总计需支付￥<strong ><?php printf("%.1f",$total_price)?></strong>元</p>
</div>

<!--

-->
</div>



</div>
