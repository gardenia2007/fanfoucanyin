<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 tdansitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Cache-Controll" content="no-cache">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<title><?=$title?></title>
	<!-- css -->
	<!-- 
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/"/>
	-->
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/common/reset.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/common/main.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/show/show.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/index/index.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/index/tags.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/common/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="<?=STATIC_ROOT?>css/common/jsorder.css"/>


	<script type='text/javascript'>
		var url = "<?=WEB_ROOT?>";
	</script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/jquery.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/jquery-ui.js"></script>
	<!-- js
	<script type='text/javascript' src="<?=STATIC_ROOT?>/js/"></script>
	 -->
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/cookie.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/jsorder.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/autoscroll.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/show.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/my.js"></script>
	<script type='text/javascript' src="<?=STATIC_ROOT?>scripts/tags.js"></script>
	<script type='text/javascript'>
$(function(){
	
	$('.tag-container').tags({
		hot_tag: "slide-tag"
	});
	
$.fn.jsorder.defaults = {
		//全局数据-保存订单信息
		staticname : 'jsorder',
		//订单class
		jsorderclass : 'jsorder',
		//是否保存cookie
		savecookie : true,
		//savecookie : false,
		//cookie的名字
		cookiename : 'jsorder',
		//ID前缀
		numpre : 'no_',
		//订单项前最
		jsorderpre : 'fanfou_', 
		//价格属性
		pricefiled : 'cp_price',
		//订单项名
		namefiled : 'cp_name',
		//购物车左侧显示
		leftdemo : '我的订单',
		//提交按钮文字
		subbutton : '',
		//默认加入订单的控件选择
		addbutton : '.addcp', 
		//没有订单时显示
		nomessage : '您还没有添加任何菜品！',
		//提交时触发
		dosubmit : function(data ,staticname) {
			$("body").append(JSON.stringify(data));
			$("body").data(staticname, {});
		},
		//url
		url: "<?=WEB_ROOT?>"
	};

	$("body").jsorder();

	$(".button").button();

	$("#loading").ajaxStart(function(){
		$(this).html("<img src='<?=STATIC_ROOT?>images/loading.gif'/><p>正在处理数据...</p>")
		.css('top',($(window).height() / 2 - 50))
		.css('left',($(window).width() / 2 - 100))
		.show();
	});
	$(window).resize(function() {
		$("#loading").css('top',($(window).height() / 2 - 50)).css('left',($(window).width() / 2 - 100));
	});



});

		
	</script>

	<!-- Google Analytics -->
	<script type='text/javascript'>
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-29875006-1']);
		  _gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	</script>

</head>

<body>

<div id="dialog_form" title="请输入您的送餐信息">
	<p id="dialog_form_tips" class="ui-corner-all"><strong>请确保您的送餐信息准确有效</strong></p>

	<form>
	<fieldset style="font-size:17px">
	<p>
		<label for="address">送餐地点*</label>
		<input type="text" name="address" placeholder="请输入您的送餐地点" id="address" class="text ui-widget-content ui-corner-all" required/>
	</p>
	<p>
		<label for="building">送餐楼宇*</label>
		<input type="text" name="building" placeholder="请输入送餐楼宇名称" id="building" class="text ui-widget-content ui-corner-all" required/>
	</p>
	<p>
		<label for="room">房间号*&nbsp;</label>
		<input type="text" name="room" placeholder="请输入送餐房间号" id="room" class="text ui-widget-content ui-corner-all" required/>
	</p>
	<p>
		<label for="phone">联系电话*</label>
		<input type="text" name="phone" placeholder="请输入您的联系电话" id="phone" class="text ui-widget-content ui-corner-all"  required/>
	</p>
	</fieldset>
	</form>
</div>

<div id="check_order_dialog" title="请确认订单信息">
	<div id="check_order_content">
 		<div>
 			<ul class="check_order_list">
 			</ul>
 		</div>
 		<div class="check_order_total"></div>
 	</div>
</div>

<div id="all">

