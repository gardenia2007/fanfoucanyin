<div id = 'content'>

<?php $count = 0?>
<?php foreach($r_info as $key => $r): ?>
<?php $count++?>
<div class='r-show <?php if($count == EACH_PAGE_SHOW) echo 'r-show-last'?>'>
	<div class='r-logo'>
		<img src='<?=$r['r_logo_url']?>'/>
	</div>
	<div class='r-detail'>
		<div class='r-name-show'>
			<div class='r-status'>
				<?php if($r['r_status'] == 0):?>
					<span class='r-status-open'>[营业中]</span>
				<?php elseif($r['r_status'] == 1):?>
					<span class='r-status-closed'>[休息中]</span>
				<?php elseif($r['r_status'] == 2):?>
					<span class='r-status-long-closed'>[长期休息]</span>
				<?php endif;?>
			</div>
			<div class='r-name'>
				<h4><a href="<?=WEB_ROOT?>show/restaurant?id=<?=$r['r_id']?>"><?=$r['r_name']?></a></h4>
			</div>
		</div>
		
		<div class='r-other-detial'>
			<div class='first-line'>
				<span class='r-type'>类型：<span><?=$r['r_type']?></span></span>
				<span class='r-delivery-price'>起送金额：<span><?=$r['r_delivery_min']?></span></span>
				<span class='r-delivery-time'>送餐时间：<span><?=$r['r_delivery_cost_time']?></span></span>
			</div>
			<div class='second-line'>
				<span class='r-special-dish'>特色菜品：<span><?=$r['r_special_dish']?></span></span>
			</div>
			<div class='third-line'>
				<span class='r-notice'>最新活动：<span><?=$r['r_activity']?></span></span>
			</div>
		</div>
	</div>
</div>

<?php endforeach;?>


<!-- 分页 -->

<div class='r-show-pagination'>

<?php $page_show = ($current_page < 3)?1:($current_page - 2);$page_show--?>
<?php for($i = 1; $i <= $total_pages && $i <= 5; $i++):?>
<span class='page'<?php if($i == ($current_page + 1 )): echo " id='current-page'"?>><?=++$page_show?></span>
<?php else :?>><a href='index?page=<?=$page_show?>'><?=++$page_show?></a></span><?php endif;?>
<?php endfor;?>

</div>


</div>

</div>

