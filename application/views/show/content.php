<div id="content">
<div id="r_show_info" r_id="<?=$r_info['r_id']?>">
<div id="r_logo">

<img src="<?=$r_info['r_logo_url']?>" alt="<?=$r_info['r_name']?>"/>

</div>

<div id="r_info">

	<h3 id="r_name"><?=$r_info['r_name']?></h3>
	<table>

		<tr>
			<td>最新活动</td>
			<td id="r_notice_activity"><?=$r_info['r_activity']?></td>
		</tr>
		<tr>
			<td>特色菜品</td>
			<td id="r_special_dish"><?=$r_info['r_special_dish']?></td>
		</tr>
		<tr>
			<td>外卖时间</td>
			<td id="r_service_time">
				<?=$r_info['r_delivery_start_time']?> - <?=$r_info['r_delivery_stop_time']?>
			</td>
		</tr>
		<tr>
			<td>配送费</td>
			<td id="r_delivery_price"><span><?=$r_info['r_delivery_price']?></span>元</td>
		</tr>
		<tr>
			<td>起送金额</td>
			<td id="r_delivery_min"><span><?=$r_info['r_delivery_min']?></span>元</td>
		</tr>
		<tr>
			<td>送餐范围</td>
			<td id="r_delivery_range"><?=$r_info['r_delivery_range']?></td>
		</tr>
		<tr>
			<td>餐厅简介</td>
			<td id="r_descript"><?=$r_info['r_descripe']?></td>
		</tr>
		<tr>
			<td>餐厅地址</td>
			<td id="r_address"><?=$r_info['r_address']?></td>
		</tr>
	</table>
	
</div>

<div style="clear:both"></div>
</div>
<div class="dish-list">
<?php

//var_dump($dish_has_logo);

?>
<?php foreach($all as $i => $t)://按类别显示所有菜品 ?> 
	<div class="cp_class">

	<?php if($dish_has_logo[$i])://如果此类别含有带logo的菜品?>
		<h3><?=$i?></h3>
	<!-- 带图片的 -->
		<div class="cp_img">

		<?php foreach($t as $j => $d):?>

			<?php if($d['d_logo_url'] != 'NULL'):?>
				<div class='pic addcp' id='<?=$d['d_id']?>' cp_name='<?=$d['d_name']?>'
					 cp_price='<?=$d['d_price']?>' cp_unit='<?=isset($d['d_unit'])?$d['d_unit']:"份"?>'>
				<div id="s_<?=$d['d_id']?>">
					<img src='<?=$d['d_logo_url']?>' alt='<?=$d['d_descripe']?>'/>
				</div>
					<span class='cp_img_add'></span>
					<span class='cp_name'><?=$d['d_name']?></span>
					<span class='cp_price'><?=$d['d_price']?>元/<?=isset($d['d_unit'])?$d['d_unit']:"份"?></span>
					
				</div>
			<?php endif;?>

		<?php endforeach;?>
		<div style="clear:both"></div>
		</div><!-- end of CP_IMG -->
	<?php endif;?>

	<?php if($dish_has_logo[$i]&& $dish_no_logo[$i])://如果此类别含有带logo的菜品?>
		<div class="cp_other">
			<h4> 还有更多 <?=$i?> 可供选择</h4>
	<?php endif;?>

	<?php if($dish_no_logo[$i])://如果此类别没有logo的菜品数量不为零?>
		<?php if(!$dish_has_logo[$i]):?>
			<div class="cp_other">
				<h3><?=$i?></h3>
		<?php endif;?>
		<!-- 不带图片的 -->
		<table>
			<?php foreach($t as $d):?>
				<?php if($d['d_logo_url'] == 'NULL'):?>
					<tr class="addcp" id='<?=$d['d_id']?>' cp_name='<?=$d['d_name']?>' cp_price='<?=$d['d_price']?>' cp_unit='<?=isset($d['d_unit'])?$d['d_unit']:"份"?>'>
						<td class='cp_show_hidden' style="border-bottom:none;"></td>
						<td class='cp_other_name'><?=$d['d_name']?></td>
						<td class='cp_other_descripe'><?=$d['d_descripe']?></td>
						<td class='cp_other_price'><?=$d['d_price']?>元/<?=isset($d['d_unit'])?$d['d_unit']:"份"?></td>
						<td class='cp_blank'></td>
					</tr>
				<?php endif;?>

			<?php endforeach;?>
		</table>

		</div><!-- end of 没有图片的 -->
	<?php endif;?>
	</div><!-- end of CP_CLASS -->

<?php endforeach;?>
</div>

</div>

