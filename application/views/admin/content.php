

				<!-- table -->
				<div class="box">
					<!-- box / title -->
					<div class="title">
						<h5>订单信息</h5>
						<div class="search">
							<form action="#" method="post">
								<div class="input">
									<input type="text" id="search" name="search" />
								</div>
								<div class="button">
									<input type="submit" name="submit" value="查找" />
								</div>
							</form>
						</div>
					</div>
					<!-- end box / title -->
					<div class="table">
						<form action="" method="post">
						<table>
							<thead>
								<tr>
									<th class="left">序号</th>
									<th>订单号</th>
									<th>联系电话</th>
									<th>送餐地址</th>
									<th>订单金额</th>
									<th>查看菜品</th>
									<th>操作</th>
									<th class="selected last"><input type="checkbox" class="checkall" /></th>
								</tr>
							</thead>
							<tbody>
							<?php $i = 1;foreach($order as $v):?>
								<tr>
								<?php var_dump($v)?>
									<td class="no"><?=$i?></td>
									<td class="sn"><?=$v['sn']?></td>
									<td class="phone"><?=$v['phone']?></td>
									<td class="address"><?=$v['address']?></td>
									<td class="price">￥<?=$v['price']?></td>
									<td class="show">查看订单菜品</td>
									<td class="act">
										<select name="action">
											<option value="" class="locked">已送餐</option>
											<option value="" class="unlocked">正在加工</option>
											<option value="" class="folder-open">放弃订单</option>
										</select>
									</td>
									<td class="selected last"><input type="checkbox" /></td>
								</tr>
							<?php $i++;endforeach?>
							</tbody>
						</table>
						<!-- pagination --
						<div class="pagination pagination-left">
							<div class="results">
								<span>showing results 1-10 of 207</span>
							</div>
							<ul class="pager">
								<li class="disabled">&laquo; prev</li>
								<li class="current">1</li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href="">4</a></li>
								<li><a href="">5</a></li>
								<li class="separator">...</li>
								<li><a href="">20</a></li>
								<li><a href="">21</a></li>
								<li><a href="">next &raquo;</a></li>
							</ul>
						</div>
						<!-- end pagination -->
						<!-- table action -->
						<div class="action" style="position:relative;height:70px">
							<div style="position:absolute;right:310px;top:15px;width:70px">
								<span>批量操作</span>
							</div>
							<div style="position:absolute;right:100px;top:6px">
								<select name="action">
									<option value="" class="locked">已送餐</option>
									<option value="" class="unlocked">正在加工</option>
									<option value="" class="folder-open">放弃订单</option>
								</select>
							</div>
							<div class="button" style="position:absolute;right:30px">
								<input type="submit" name="submit" value="应用" />
							</div>
						</div>
						<!-- end table action -->
						</form>
					</div>
				</div>
				<!-- end table -->
