<div class="box">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></span></h5>
		
		<div class="search">
		
			<form action="#" method="post">
				<div class="input">
					<input type="text" id="search" name="search" />
				</div>
				<div class="button">
					<input type="submit" name="submit" value="查找" />
				</div>
			</form>
		</div>
	</div>
	<!-- end box / title -->

<div class="messages">
	
	<?php if(isset($act_success) && $act_success):?>
	<div class="messages">
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span></span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	<?php elseif(isset($act_success) && !$act_success): ?>
	<div class="messages">
	<div id="error-success" class="message message-error">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/error.png" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>失败!</h6>
			<span><?=$msg?></span>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
</div>
	
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('admin/add_r') ?>
<div class="form">
<div class="fields">
	<div class="field field-first">
		<div class="label">
			<label for="name">餐厅名称：</label>
		</div>
		<div class="input">
			<input name="name" type="text" class="small" value="<?=set_value("name")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="activity">最新活动：</label>
		</div>
		<div class="input">
			<input name="activity" type="text" class="medium" value="<?=set_value("activity")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="url">自定义域名：</label>
		</div>
		<div class="input">
			<input name="url" type="text" class="medium" value="<?=set_value("url")?>"/>
			<span>（此项限最多32位的英文字母与数字组合）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="status">营业状态：</label>
		</div>
		<div class="select">
			<select name="status" class="small" required>
				<option value='0'>正常营业</option>
				<option value='1'>休息中</option>
				<option value='2'>长期休息</option>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="service_name">营业时间：</label>
		</div>
		<div class="input">
			<input name="service_time" type="text" class="small" required value="<?=set_value("service_time")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="type">类别（标签）：</label>
		</div>
		<div class="input">
			<input name="type" type="text" class="small"  required value="<?=set_value("type")?>"/>
			<span>（不同的标签请用空格分开）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="special_dish">特色菜品：</label>
		</div>
		<div class="input">
			<input name="special_dish" type="text" class="small"  value="<?=set_value("special_dish")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_price">送餐费用：</label>
		</div>
		<div class="input">
			<input name="delivery_price" type="text" class="small"  value="<?=set_value("delivery_price")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_min">送餐最少订单金额：</label>
		</div>
		<div class="input">
			<input name="delivery_min" type="text" class="small" value="<?=set_value("delivery_min")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_range">送餐区域：</label>
		</div>
		<div class="input">
			<input name="delivery_range" type="text" class="small"  value="<?=set_value("delivery_range")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_start_time">送餐开始时间：</label>
		</div>
		<div class="input">
			<input name="delivery_start_time" type="text" class="small"  value="<?=set_value("delivery_start_time")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_stop_time">送餐结束时间：</label>
		</div>
		<div class="input">
			<input name="delivery_stop_time" type="text" class="small"  value="<?=set_value("delivery_stop_time")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="phone">联系电话：</label>
		</div>
		<div class="input">
			<input name="phone" type="text" class="small"  required  value="<?=set_value("phone")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="address">餐厅地址：</label>
		</div>
		<div class="input">
			<input name="address" type="text" class="medium" value="<?=set_value("address")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="pic">餐厅图标：</label>
		</div>
		<div class="input input-file">
			<input name="pic" type="file"  class="file"/>
		</div>
	</div>
	<div class="field">
		<div class="label label-textarea">
			<label for="descripe">餐厅简介：</label>
		</div>
		<div class="textarea ">
			<textarea name="descripe" cols="30" rows="5" class="editor" style=" height:100px;" ></textarea>
		</div>
	</div>
	<div class="field"></div>
	<div class="field">
		<div class="label">
			<label for="username">管理用户名：</label>
		</div>
		<div class="input">
			<input name="username" type="text" class="small"  value="<?=set_value("username")?>"/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="passwd">管理密码：</label>
		</div>
		<div class="input">
			<input name="passwd" type="text" class="small"  value="<?=set_value("passwd")?>"/>
		</div>
	</div>
	<div class="buttons">
		<input type="submit" value="确认修改"/>
		<input type="reset" value="重新填写"/>
	</div>
</div>
</div>
</form>

</div>