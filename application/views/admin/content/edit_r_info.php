<div class="box">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></span></h5>
		
		<div class="search">
		
			<form action="#" method="post">
				<div class="input">
					<input type="text" id="search" name="search" />
				</div>
				<div class="button">
					<input type="submit" name="submit" value="查找" />
				</div>
			</form>
		</div>
	</div>
	<!-- end box / title -->
		
<div class="messages">
	
	<?php if(isset($act_success) && $act_success):?>
	<div class="messages">
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span></span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	<?php elseif(isset($act_success) && !$act_success): ?>
	<div class="messages">
	<div id="error-success" class="message message-error">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/error.png" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>失败!</h6>
			<span><?=$msg?></span>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
</div>
	
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('admin/edit_r_info_post') ?>
<div class="form">
<div class="fields">
	<div class="field field-first">
		<div class="label">
			<label for="name">餐厅名称：</label>
		</div>
		<div class="input">
			<input name="name" type="text" class="small" value="<?=$r_name?>" disabled required/>
			<span>（如需修改此项，请与管理员联系）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="activity">最新活动：</label>
		</div>
		<div class="input">
			<input name="activity" type="text" class="medium" value="<?=$r_activity?>"/>
			<span>（此项请填入语言描述内容）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="url">自定义域名：</label>
		</div>
		<div class="input">
			<input name="url" type="text" class="medium" value="<?=$r_url?>"/>
			<span>（此项限最多32位的英文字母与数字组合）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="status">营业状态：</label>
		</div>
		<div class="select">
			<select name="status" class="small" required>
				<option value='0' <?=($r_status==0)?"selected='selected'":""?>>正常营业</option>
				<option value='1' <?=($r_status==1)?"selected='selected'":""?>>休息中</option>
				<option value='2' <?=($r_status==2)?"selected='selected'":""?>>长期休息</option>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="service_name">营业时间：</label>
		</div>
		<div class="input">
			<input name="service_time" type="text" class="small" value="<?=$r_service_time?>" required/>
			<span>（请填入一个时间段，例如8：00-18：00）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="type">类别（标签）：</label>
		</div>
		<div class="input">
			<input name="type" type="text" class="small" value="<?=$r_type?>" required/>
			<span>（不同的标签请用空格分开）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="special_dish">特色菜品：</label>
		</div>
		<div class="input">
			<input name="special_dish" type="text" class="small" value="<?=$r_special_dish?>"/>
			<span>（请填写菜品名）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_price">配送费：</label>
		</div>
		<div class="input">
			<input name="delivery_price" type="text" class="small" value="<?=$r_delivery_price?>" required/>
			<span>（此项为金额，只能填入数字，单位为“元”）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_min">送餐最少订单金额：</label>
		</div>
		<div class="input">
			<input name="delivery_min" type="text" class="small" value="<?=$r_delivery_min?>" required/>
			<span>（此项为金额，只能填入数字，单位为“元”）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_range">送餐区域：</label>
		</div>
		<div class="input">
			<input name="delivery_range" type="text" class="small" value="<?=$r_delivery_range?>" required/>
			<span>（此项请填入语言描述内容）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_range">配送平均时间：</label>
		</div>
		<div class="input">
			<input name="delivery_cost_time" type="text" class="small" value="<?=$r_delivery_cost_time?>" required/>
			<span>（此项请填入语言描述内容）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_start_time">送餐开始时间：</label>
		</div>
		<div class="input">
			<input name="delivery_start_time" type="text" class="small" value="<?=$r_delivery_start_time?>" required/>
			<span>（请填写一个时间点，例如10:00）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="delivery_stop_time">送餐结束时间：</label>
		</div>
		<div class="input">
			<input name="delivery_stop_time" type="text" class="small" value="<?=$r_delivery_stop_time?>" required/>
			<span>（请填写一个时间点，例如18:00）</span>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="phone">联系电话：</label>
		</div>
		<div class="input">
			<input name="phone" type="text" class="small" value="<?=$r_phone?>" required/>
			<span>（请填写一个能收到订单短信通知的手机号码）</span>
		</div>
	</div>
	<!-- 
	<div class="field">
		<div class="label">
			<label for="phone">联系电话：</label>
		</div>
		<div class="input">
			<input name="phone" type="text" class="small" value="<?=$r_phone?>" required/>
			<span>（请填写一个能收到订单短信通知的手机号码）</span>
		</div>
	</div>
	-->
	<div class="field">
		<div class="label">
			<label for="address">餐厅地址：</label>
		</div>
		<div class="input">
			<input name="address" type="text" class="medium" value="<?=$r_address?>" required/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="pic">餐厅图标：</label>
		</div>
		<div class="input input-file">
			<input name="pic" type="file" value="<?=$r_logo_url?>" class="file"/>
			<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;（请点击右边的图标选择一个图片上传，如不需修改请忽略此项）</span>
		</div>
	</div>
	<div class="field">
		<div class="label label-textarea">
			<label for="descripe">餐厅简介：</label>
		</div>
		<div class="textarea ">
			<textarea name="descripe" cols="30" rows="5" class="editor" style=" height:100px;" ><?=$r_descripe?></textarea>
		</div>
	</div>	
	<div class="buttons">
		<input type="submit" value="确认修改"/>
		<input type="reset" value="重新填写"/>
	</div>
</div>
</div>
</form>

</div>