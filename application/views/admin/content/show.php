

<!-- table -->
<div class="box">
	<!-- box / title -->
	<div class="title">
		<h5>订单信息(<?=$date?>)</h5>
		
		<div class="search">
			<form action="#" method="post">
				<div class="input">
					<input type="text" id="search" name="search" />
				</div>
				<div class="button">
					<input type="submit" name="submit" value="查找" />
				</div>
			</form>
		</div>
	</div>
		<?php if(count($order) == 0):?>
		<div class="messages">
		<div id="message-notice" class="message message-notice">
			<div class="image">
				<img src="<?=$url?>resources/images/icons/notice.png" alt="Notice" height="32" />
			</div>
			<div class="text">
				<h6>提示信息</h6>
				<span><?=$date?> 没有订单信息！</span>
			</div>
			<div class="dismiss">
				<a href="#message-notice"></a>
			</div>
		</div>
		</div>
		<?php else: ?>
	<!-- end box / title -->
	<div class="table">
		<form action="" method="post">
		<table id="jstable">
			<thead>
				<tr>
					<th>序号</th>
					<th>订单号</th>
					<th>联系电话</th>
					<th>送餐地址</th>
					<th>订单金额</th>
					<th>查看菜品</th>
					<th>操作</th>
					<th class="selected last"><input type="checkbox" class="checkall" /></th>
				</tr>
			</thead>
			<tbody>
			<?php $i = 1;foreach($order as $v):?>

				<tr>
					<td class="no"><?=$v['time']?></td>
					<td class="sn"><?=$v['sn']?></td>
					<td class="phone"><?=$v['phone']?></td>
					<td class="address"><?=$v['address']?> <?=$v['building']?> <?=$v['room']?></td>
					<td class="price">￥<?=$v['price']?></td>
					<td class="show"><a>查看订单菜品</a></td>
					<td class="act">
						<select name="action">
							<option value="" class="locked">已送餐</option>
							<option value="" class="unlocked">正在加工</option>
							<option value="" class="folder-open">放弃订单</option>
						</select>
					</td>
					<td class="selected last"><input type="checkbox" /></td>
				</tr>
				<tr>
					<td></td>
					<td colspan="5">
						<table class = "show_in_show">
						<?php foreach($v['detail'] as $key => $d):?>
						<?php if($key % 2 == 0):?>
							<tr>
								<td width="35%"><?=$d['d_name']?></td>
								<td width="15%" style="border-right:1px dashed #ccc"><?=$d['d_num']?>份</td>
						<?php else:?>
								<td width="35%"><?=$d['d_name']?></td>
								<td width="15%"><?=$d['d_num']?>份</td>
							</tr>
						<?php endif;?>
						<?php endforeach;?>
							</tr>
						</table>
					</td>
					<td></td>
				</tr>
			<?php $i++;endforeach?>
			</tbody>
		</table>
		<!-- pagination --
		
		
						<table>
							<?php foreach($v['detail'] as $d):?>
							<tr>
								<td><?=$d['d_name']?></td>
								<td><?=$d['d_price']?></td>
								<td><?=$d['d_num']?></td>
							</tr>
							<?php endforeach;?>
						</table>
			
		
		
						<div class="pagination pagination-left">
							<div class="results">
								<span>showing results 1-10 of 207</span>
							</div>
							<ul class="pager">
								<li class="disabled">&laquo; prev</li>
								<li class="current">1</li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href="">4</a></li>
								<li><a href="">5</a></li>
								<li class="separator">...</li>
								<li><a href="">20</a></li>
								<li><a href="">21</a></li>
								<li><a href="">next &raquo;</a></li>
							</ul>
						</div>
						<!-- end pagination -->
						<!-- table action -->
		<div class="action" style="position:relative;height:70px">
			<div style="position:absolute;right:310px;top:15px;width:70px">
				<span>批量操作</span>
			</div>
			<div style="position:absolute;right:100px;top:6px">
				<select name="action">
					<option value="" class="locked">已送餐</option>
					<option value="" class="unlocked">正在加工</option>
					<option value="" class="folder-open">放弃订单</option>
				</select>
			</div>
			<div class="button" style="position:absolute;right:30px">
				<input type="submit" name="submit" value="应用" />
			</div>
		</div>
		<!-- end table action -->
		</form>
	</div>
	<?php endif ?>
</div>
<!-- end table -->

<div class="box">
	<div class="title">
		<h5><?=$date?> 订单统计</h5>
	</div>
	
	<br/>
	<br/>
</div>

