
<!-- -->


<div class="box box-left" style="width:69%">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></h5>
		
		<div class="search">
		
			<form action="#" method="post">
				<div class="input">
					<input type="text" id="search" name="search" />
				</div>
				<div class="button">
					<input type="submit" name="submit" value="查找" />
				</div>
			</form>
		</div>
	</div>
	<!-- end box / title -->
	
	<?php if(isset($act_success) && $act_success):?>
	<div class="messages">
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span></span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	<?php elseif(isset($act_success) && !$act_success): ?>
	<div class="messages">
	<div id="error-success" class="message message-error">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/error.png" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>失败!</h6>
			<span><?=$msg?></span>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
	<?php if(!isset($id)):?>
	<div class="messages">
	<div id="message-notice" class="message message-notice">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/notice.png" alt="提示" height="32" />
		</div>
		<div class="text">
			<h6>提示</h6>
			<span>请先在右边点击选择需要修改信息的菜品</span>
		</div>
		<div class="dismiss">
			<a href="#message-notice"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
	
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('admin/edit_dish_post') ?>
<div class="form my_form_slide">
<div class="fields">
	<div class="field field-first">
		<div class="label">
			<label for="name">菜品名：</label>
		</div>
		<div class="input">
			<input name="name" type="text" class="small" value="<?=isset($d_name)?$d_name:set_value('name')?>" required/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="type">菜品类别：</label>
		</div>
		<div class="select">
			<select name="type" class="small" required>
			<?php foreach($all_type as $v):?>
				<option value='<?=$v['t_id']?>' <?=(isset($d_type)&&$v['t_id']==$d_type)?"selected='selected'":"a"?>><?=$v['t_name']?></option>
			<?php endforeach;?>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="name">菜品价格：</label>
		</div>
		<div class="input">
			<input name="price" type="text" class="small" value="<?=isset($d_price)?$d_price:set_value('price')?>" required/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="name">菜品描述(20字以内)：</label>
		</div>
		<div class="input ">
			<input name="descripe" type="text" class="medium" value="<?=isset($d_descripe)?$d_descripe:set_value('descripe')?>" required/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="pic">菜品图片(如需修改图片，请重新选择图片上传，否则请保持此项为空)：</label>
		</div>
		<div class="input input-file">
			<input name="pic" type="file" value="<?=isset($d_logo_url)?$d_logo_url:set_value('pic')?>" class="file"/>
		</div>
	</div>
	<div class="buttons">
		<input type="submit" value="确认添加"/>
		<input type="reset" value="重新填写"/>
	</div>
	<input type="hidden" name="d_id" value="<?=isset($id)?$id:''?>"/>
</div>
</div>
</form>

</div>