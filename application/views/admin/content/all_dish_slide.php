
<!-- -->
<div class="box box-right" style="width:29%">
	<!-- box / title -->
	<div class="title">
		<h5>当前已存在类别和菜品</h5>
	</div>
	
	<div>
		<ul class="disc" id="sortable">
		<?php foreach($all_dish as $i => $v):?>
			<li>
				<dl>
					<dt>
						<a href="<?=$url?>admin/edit_type/<?=$all_type[$i]['t_id']?>"><?=$all_type[$i]['t_name']?></a>
					</dt>
					<?php foreach($v as $d):?>
					<dd>
						<a href="<?=$url?>admin/edit_dish/<?=$d['d_id']?>"><?=$d['d_name']?></a>
					</dd>
					<?php endforeach;?>
				</dl>
			</li>
		<?php endforeach;?>

		</ul>
	</div>
	
</div>