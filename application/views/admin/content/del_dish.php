

<!-- -->

<div class="box box-left" style="width:69%">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></h5>
		
		<div class="search">
		
			<form action="#" method="post">
				<div class="input">
					<input type="text" id="search" name="search" />
				</div>
				<div class="button">
					<input type="submit" name="submit" value="查找" />
				</div>
			</form>
		</div>
	</div>
	<!-- end box / title -->
	
	<?php if(isset($act_success) && $act_success):?>
	<div class="messages">
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span></span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	<?php elseif(isset($act_success) && !$act_success): ?>
	<div class="messages">
	<div id="error-success" class="message message-error">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/error.png" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>失败!</h6>
			<span>请稍候再试！</span>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
	
	
<?php echo validation_errors(); ?>
<?php echo form_open('admin/del_dish') ?>
<div class="form my_form_slide">
<div class="fields">
	
	<div class="field field-first">
		<div class="label label-checkbox">
			<label for="name">菜品名：</label>
		</div>
		<div class="checkboxes">

		<ul class="disc">
		<?php foreach($all_dish as $i => $v):?>
			<li>
				<dl>
					<dt><?=$all_type[$i]['t_name']?></dt>
					<?php foreach($v as $d):?>
						<dd>
							<input type="checkbox" name="dish[]" value="<?=$d['d_id']?>"/>
							<label><?=$d['d_name']?></label>
						</dd>
					<?php endforeach;?>
				</dl>
			</li>
		<?php endforeach;?>
		</ul>

		</div>
	</div>
	<div class="buttons">
		<input type="submit" value="确认删除"/>
	</div>
</div>
</div>
</form>

</div>