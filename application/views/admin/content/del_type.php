
<!-- -->

<div class="box box-left" style="width:69%">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></h5>
		
	</div>
	<!-- end box / title -->
	
	<div class="messages">
	<?php if(isset($act_success) AND $act_success):?>
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span></span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>	
	</div>
	<?php endif ?>
	<div id="message-warning" class="message message-warning">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/warning.png" alt="Warning" height="32" />
		</div>
		<div class="text">
			<h6>警告！</h6>
			<span>删除类别会使该类别下的菜品不能显示！</span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	
	
<?php echo validation_errors(); ?>
<?php echo form_open('admin/del_type') ?>
<div class="form my_form_slide">
<div class="fields">
	<div class="field field-first">
		<div class="label">
			<label for="type">菜品类别：</label>
		</div>
		<div class="select">
			<select name="type" class="small" required>
			<?php foreach($all_type as $v):?>
				<option value='<?=$v['t_id']?>'><?=$v['t_name']?></option>
			<?php endforeach;?>
			</select>
		</div>
	</div>
	<div class="buttons">
		<input type="submit" value="确认删除"/>
	</div>
</div>
</div>
</form>

</div>