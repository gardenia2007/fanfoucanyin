
<script type="text/javascript">
	$(function(){
		$("#sortable").sortable({
			axis:'y',
			placeholder:'ui-state-highlight',
			cursor:'pointer',
			start:function(event, ui){
				//$(this).children('li').hide();
			},
			stop:function(event, ui){
				//$(this).children('li').show();
			}
		}).disableSelection();
		
	});
</script>

<div class="box box-left" style="width:69%">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></h5>
		
	</div>
	<!-- end box / title -->
	
	<div class="messages">
	
	<?php if(isset($act_success) && $act_success):?>
	<div class="messages">
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span></span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	<?php elseif(isset($act_success) && !$act_success): ?>
	<div class="messages">
	<div id="error-success" class="message message-error">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/error.png" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>失败!</h6>
			<span>请稍候再试！</span>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
		<div id="message-notice" class="message message-notice">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/notice.png" alt="提示" height="32" />
		</div>
		<div class="text">
			<h6>提示</h6>
			<span>请先在右边点击选择需要修改信息的类别</span>
		</div>
		<div class="dismiss">
			<a href="#message-notice"></a>
		</div>
	</div>
	</div>
	
	
<?php echo validation_errors(); ?>
<?php echo form_open('admin/edit_type_post') ?>
<div class="form my_form_slide">
<div class="fields">
	<div class="field field-first">
		<div class="label">
			<label for="name">类别名：</label>
		</div>
		<div class="input">
			<input name="name" type="text" class="small" value="<?=isset($t_name)?$t_name:set_value('name')?>" required/>
		</div>
	</div>

	<div class="field">
		<div class="label label-textarea">
			<label for="name">类别描述：</label>
		</div>
		<div class="textarea ">
			<textarea name="descripe" cols="30" rows="5" class="editor" style=" height:100px;" required><?=isset($t_descripe)?$t_descripe:set_value('descripe')?></textarea>
		</div>
	</div>	
	
	<input type="hidden" name="id" value="<?=isset($id)?$id:""?>"/>

	<div class="buttons">
		<input type="submit" value="确认添加"/>
		<input type="reset" value="重新填写"/>
	</div>
</div>
</div>
</form>
</div>