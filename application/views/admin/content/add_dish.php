
<!-- -->

<div class="box box-left" style="width:69%">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></h5>
		
		<div class="search">
		
			<form action="#" method="post">
				<div class="input">
					<input type="text" id="search" name="search" />
				</div>
				<div class="button">
					<input type="submit" name="submit" value="查找" />
				</div>
			</form>
		</div>
	</div>
	<!-- end box / title -->
	
	<?php if($msg !== FALSE && $act_success):?>
	<div class="messages">
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span>如需继续添加请继续输入菜品信息</span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	<?php elseif($msg !== FALSE && !$act_success): ?>
	<div class="messages">
	<div id="error-success" class="message message-error">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/error.png" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>失败!</h6>
			<span><?=$msg?></span>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
	
<?php echo validation_errors(); ?>
<?php echo form_open_multipart('admin/add_dish') ?>
<div class="form my_form_slide">
<div class="fields">
	<div class="field field-first">
		<div class="label">
			<label for="name">菜品名：</label>
		</div>
		<div class="input">
			<input name="name" type="text" class="small" value="<?=$act_success?"":set_value('name')?>"required/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="type">菜品类别：</label>
		</div>
		<div class="select" style='margin-left:60px;'>
			<select name="type" class="small" required>
			<?php foreach($all_type as $v):?>
				<option value='<?=$v['t_id']?>'><?=$v['t_name']?></option>
			<?php endforeach;?>
			</select>
		</div>
	</div>

	<div class="field">
		<div class="label">
			<label for="name">菜品价格：</label>
		</div>
		<div class="input">
			<input name="price" type="text" class="small" value="<?=$act_success?"":set_value('price')?>"required/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="name">菜品描述(20字以内)：</label>
		</div>
		<div class="input ">
			<input name="descripe" type="text" class="medium" required/>
		</div>
	</div>	
	<div class="field">
		<div class="label">
			<label for="pic">菜品图片：</label>
		</div>
		<div class="input input-file">
			<input name="pic" type="file"/>
		</div>
	</div>
	<div class="buttons">
		<input type="submit" value="确认添加"/>
		<input type="reset" value="重新填写"/>
	</div>
</div>
</div>
</form>

</div>