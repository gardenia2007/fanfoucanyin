<script type="text/javascript">

			$(document).ready(function () {

				$(".my-button").hover(function(){
					$(this).children('input:submit').attr('class','button-hover');
				},function(){
					$(this).children('input:submit').attr('class','button-default');
				});

			});

</script>



<div class="box">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></h5>
		<div class="search">
			<form action="#" method="post">
				<div class="input">
					<input type="text" id="search" name="search" />
				</div>
				<div class="button">
					<input type="submit" name="submit" value="查找" />
				</div>
			</form>
		</div>
	</div>		
	<div>
	</div>
	
	<div style="height:300px;margin:100px">
		<?php echo validation_errors(); ?>
		<?php echo form_open('admin/show') ?>
		<div class="form">
		<div class="fields">
		<div class=" field field-first">
			<div class="label" style="left:450px">
				<label for='date'>请选择显示订单的日期：</label>
			</div>
			<div class="input">
				<input type="text" id="date" name="date" style="width:200px" class="date-picker"/>
			</div>
			<div class="my-button" style="display:inline;margin-left:20px;">
				<input class='button-default'type="submit" name="submit" value="显示订单"/>
			</div>
		</div>
		</div>
		</div>
		</form>
	</div>
</div>
