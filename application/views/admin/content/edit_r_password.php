<div class="box">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></span></h5>
		
		<div class="search">
		
			<form action="#" method="post">
				<div class="input">
					<input type="text" id="search" name="search" />
				</div>
				<div class="button">
					<input type="submit" name="submit" value="查找" />
				</div>
			</form>
		</div>
	</div>
	<!-- end box / title -->
		
	<?php if(isset($act_success) && $act_success):?>
	<div class="messages">
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span></span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	<?php elseif(isset($act_success) && !$act_success): ?>
	<div class="messages">
	<div id="error-success" class="message message-error">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/error.png" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>失败!</h6>
			<?php if(isset($msg)):?>
			<span><?=$msg?></span>
			<?php else:?>
			<span>请稍候再试！</span>
			<?php endif;?>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
	
<?php echo validation_errors(); ?>
<?php echo form_open('admin/edit_r_password') ?>
<div class="form">
<div class="fields">
	<div class="field field-first">
		<div class="label">
			<label for="old_password">原密码：</label>
		</div>
		<div class="input">
			<input name="old_password" type="password" class="small" required/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="new_password">新密码：</label>
		</div>
		<div class="input">
			<input name="new_password" type="password" class="small" required/>
		</div>
	</div>
	<div class="field">
		<div class="label">
			<label for="new_password2">再输入一次新密码：</label>
		</div>
		<div class="input">
			<input name="new_password2" type="password" class="small" required/>
		</div>
	</div>
	<div class="buttons">
		<input type="submit" value="确认修改"/>
		<input type="reset" value="重新填写"/>
	</div>
</div>
</div>
</form>

</div>