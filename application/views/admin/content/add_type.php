
<!-- -->

<div class="box box-left" style="width:69%">
	<!-- box / title -->
	<div class="title">
		<h5><?=$title?></h5>
		
	</div>
	<!-- end box / title -->
	
	
	<?php if($msg !== FALSE && $act_success):?>
	<div class="messages">
	<div id="message-success" class="message message-success">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/success.png" alt="Success" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>成功!</h6>
			<span>如需继续添加请继续输入菜品信息</span>
		</div>
		<div class="dismiss">
			<a href="#message-success"></a>
		</div>
	</div>
	</div>
	<?php elseif($msg !== FALSE && !$act_success): ?>
	<div class="messages">
	<div id="error-success" class="message message-error">
		<div class="image">
			<img src="<?=$url?>resources/images/icons/error.png" alt="Error" height="32" />
		</div>
		<div class="text">
			<h6><?=$title?>失败!</h6>
			<span><?=$msg?></span>
		</div>
		<div class="dismiss">
			<a href="#message-error"></a>
		</div>
	</div>
	</div>
	<?php endif;?>
	
<?php echo validation_errors(); ?>
<?php echo form_open('admin/add_type') ?>
<div class="form my_form_slide">
<div class="fields">
	<div class="field field-first">
		<div class="label">
			<label for="name">类别名：</label>
		</div>
		<div class="input">
			<input name="name" type="text" class="small" value="<?=set_value('name')?>"required/>
		</div>
	</div>

	<div class="field">
		<div class="label label-textarea">
			<label for="name">类别描述：</label>
		</div>
		<div class="textarea ">
			<textarea name="descripe" cols="30" rows="5" class="editor" style=" height:100px;" required></textarea>
		</div>
	</div>	

	<div class="buttons">
		<input type="submit" value="确认添加"/>
		<input type="reset" value="重新填写"/>
	</div>
</div>
</div>
</form>
</div>


