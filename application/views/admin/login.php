<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?=$title?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<!-- stylesheets -->
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>resources/css/admin/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>resources/css/admin/style.css" media="screen" />
		<link id="color" rel="stylesheet" type="text/css" href="<?=base_url()?>resources/css/colors/blue.css" />
		<!-- scripts (jquery) -->
		<script src="<?=base_url('resources/scripts/jquery-1.4.2.min.js')?>" type="text/javascript"></script>
		<script src="<?=base_url()?>resources/scripts/jquery-ui-1.8.custom.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>resources/scripts/smooth.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function () {
				style_path = "resources/css/colors";
/*
				$("input.focus").focus(function () {
					if (this.value == this.defaultValue) {
						this.value = "";
					}
					else {
						this.select();
					}
				});

				$("input.focus").blur(function () {
					if ($.trim(this.value) == "") {
						this.value = (this.defaultValue ? this.defaultValue : "");
					}
				});
*/
				$("input:submit, input:reset").button();
			});
		</script>
	</head>
	<body>
		<div id="login">
			<!-- login -->
			<div class="title">
				<h5><?=$title?></h5>
				<div class="corner tl"></div>
				<div class="corner tr"></div>
			</div>
			<?php if(isset($failed) && $failed == TRUE):?>
				<div class="messages"><div id="message-error" class="message message-error">
				<div class="image"><img src="http://localhost/admin_panel/resources/images/icons/error.png" alt="Error" height="32" /></div>
				<div class="text"><h6>错误信息</h6><span>用户名或密码错误，请检查输入！</span></div>
				<div class="dismiss"><a href="#message-error"></a></div></div></div>
			<?php endif ?>
			<?php echo validation_errors(); ?>
			<div class="inner">
				<?php echo form_open('admin/login') ?>
				<!--<form action="index.html" method="post">-->
				<div class="form">
					<!-- fields -->
					<div class="fields">
						<div class="field">
							<div class="label">
								<label for="username">用户名</label>
							</div>
							<div class="input">
								<input type="text" id="username" name="username" size="40" value="<?php echo set_value('username'); ?>" class="focus" />
							</div>
						</div>
						<div class="field">
							<div class="label">
								<label for="password">密码</label>
							</div>
							<div class="input">
								<input type="password" id="password" name="password" size="40" value="<?php echo set_value('password'); ?>" class="focus" />
							</div>
						</div>
						<div class="field">
							<div class="checkbox">
								<input type="checkbox" id="remember" name="remember" />
								<label for="remember">记住我</label>
							</div>
						</div>
						<div class="buttons">
							<input type="submit" value="登陆" />
						</div>
					</div>
					<!-- end fields -->
					<!-- links -->
					<div class="links">
						<a href="#">忘记用户名或密码请与管理员联系</a>
					</div>
					<!-- end links -->
				</div>
				</form>
			</div>
			<!-- end login -->
			<div id="colors-switcher" class="color">
				<a href="" class="blue" title="Blue"></a>
				<a href="" class="green" title="Green"></a>
				<a href="" class="brown" title="Brown"></a>
				<a href="" class="purple" title="Purple"></a>
				<a href="" class="red" title="Red"></a>
				<a href="" class="greyblue" title="GreyBlue"></a>
			</div>
		</div>
	</body>
</html>