		<!-- content -->
		<div id="content">
			<!-- end content / left -->
			<div id="left">
				<div id="menu">
					<h6 id="h-menu-products" class="selected"><a href="#products"><span>查看订单</span></a></h6>
					<ul id="menu-products" class="opened">
						<li><a href="<?=$url?>admin/show/<?=$today?>">今日订单</a></li>
						<!-- 
						<li><a href="<?=$url?>admin/show/<?=$tomorrow?>">明日订单</a></li>
						-->
						<li><a href="<?=$url?>admin/show">按日期显示订单</a></li>
					</ul>
					<h6 id="h-menu-pages"><a href="#pages"><span>修改菜品信息</span></a></h6>
					<ul id="menu-pages" class="opened">
						<li class="collapsible">
							<a href="#" class="plus">菜品</a>
							<ul class="collapsed" style="display:none">
								<li><a href="<?=$url?>admin/add_dish">添加菜品</a></li>
								<li><a href="<?=$url?>admin/edit_dish">修改菜品信息</a></li>
								<li class="last"><a href="<?=$url?>admin/del_dish">删除菜品</a></li>
							</ul>
						</li>
						<li class="collapsible last">
							<a href="#" class="plus">类别</a>
							<ul class="collapsed" style="display:none">
								<li><a href="<?=$url?>admin/add_type">添加菜品类别</a></li>
								<li><a href="<?=$url?>admin/edit_type">修改类别信息</a></li>
								<li class="last"><a href="<?=$url?>admin/del_type">删除类别</a></li>
							</ul>
						</li>
					</ul>
					<h6 id="h-menu-events"><a href="#events"><span>修改餐厅信息</span></a></h6>
					<ul id="menu-events" class="opened">
						<li><a href="<?=$url?>admin/edit_r_info">修改基本信息</a></li>
						<li class="last"><a href="<?=$url?>admin/edit_r_password">修改后台登陆密码</a></li>
					</ul>
					<?php if(isset($all_r)):?>
					<!-- 只有超级管理员才能看到 -->
					<h6 id="h-menu-links"><a href="#links"><span>超级管理员</span></a></h6>
					<ul id="menu-links" class="opened" style="display:none">
						<li><a href="<?=$url?>admin/add_r">添加餐馆</a></li>
					</ul>
					<?php endif;?>
					<!-- 后续功能 --
					<h6 id="h-menu-links"><a href="#links"><span>历史统计信息</span></a></h6>
					<ul id="menu-links" class="closed" style="display:none">
						<li><a href="">正在添加中。。。</a></li>
						<li class="last"><a href="">正在添加中。。。</a></li>
					</ul>
					<h6 id="h-menu-settings"><a href="#settings"><span>其他</span></a></h6>
					<ul id="menu-settings" style="display:none">
						<li><a href="">Manage Settings</a></li>
						<li class="last"><a href="">New Setting</a></li>
					</ul>
					<!-- 后续功能 -->
				</div>
				<div class="date-picker"></div>
			</div>
			<!-- end content / left -->
			
			<!-- content / right -->
			<div id="right">