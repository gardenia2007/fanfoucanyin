<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<head>
		<title><?=$title?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<!-- stylesheets -->
		<link rel="stylesheet" type="text/css" href="<?=$url?>resources/css/admin/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?=$url?>resources/css/admin/style.css" media="screen" />
		<link id="color" rel="stylesheet" type="text/css" href="<?=$url?>resources/css/colors/blue.css" />
		<link rel="stylesheet" type="text/css" href="<?=$url?>resources/css/admin/admin.css" />
		<!-- scripts (jquery) -->
		<script src="<?=$url?>resources/scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
		<!--[if IE]><script language="javascript" type="text/javascript" src="resources/scripts/excanvas.min.js"></script><![endif]-->
		<script src="<?=$url?>resources/scripts/jquery-ui-1.8.custom.min.js" type="text/javascript"></script>
		<script src="<?=$url?>resources/scripts/jquery.ui.selectmenu.js" type="text/javascript"></script>
		<!-- 暂时不需要
		画图
		<script src="resources/scripts/jquery.flot.min.js" type="text/javascript"></script>
		<script src="resources/scripts/smooth.chart.js" type="text/javascript"></script>
		文本编辑框
		<script src="resources/scripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
		<script src="resources/scripts/tiny_mce/jquery.tinymce.js" type="text/javascript"></script>
		
		<script src="resources/scripts/smooth.autocomplete.js" type="text/javascript"></script>
		-->
		<!-- scripts (custom) -->
		<script src="<?=$url?>resources/scripts/smooth.js" type="text/javascript"></script>
		<script src="<?=$url?>resources/scripts/smooth.menu.js" type="text/javascript"></script>
		<script src="<?=$url?>resources/scripts/smooth.table.js" type="text/javascript"></script>
		<script src="<?=$url?>resources/scripts/smooth.form.js" type="text/javascript"></script>
		<script src="<?=$url?>resources/scripts/smooth.dialog.js" type="text/javascript"></script>
		<script src="<?=$url?>resources/scripts/jstable.js" type="text/javascript"></script>
		<script src="<?=$url?>resources/scripts/jquery.ui.datepicker-zh-CN.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function () {
				style_path = "resources/css/colors";

				$(".date-picker").datepicker();

				$("#box-tabs, #box-left-tabs").tabs();

				$("#jstable").jstable();
			});
		</script>
	</head>
	<body>
		<div id="colors-switcher" class="color">
			<a href="" class="blue" title="Blue"></a>
			<a href="" class="green" title="Green"></a>
			<a href="" class="brown" title="Brown"></a>
			<a href="" class="purple" title="Purple"></a>
			<a href="" class="red" title="Red"></a>
			<a href="" class="greyblue" title="GreyBlue"></a>
		</div>

		<!-- header -->
		<div id="header">
			<!-- logo -->
			<div id="logo">
				<h1><a href="" title="<?=$title?>"><?=$title?></a></h1>
			</div>
			<!-- end logo -->
			<!-- user -->
			<ul id="user">
				<li class="first"><a href="<?=$url?>admin/info">查看账户详细信息</a></li>
				<li><a href="<?=$url?>admin/logout">注销登陆</a></li>
				<li class="highlight last"><a href="<?=$url?>">返回首页</a></li>
			</ul>
			<!-- end user -->
			<div id="header-inner">
				<div id="home">
					<a href="<?=WEB_ROOT?>" title="管理首页"></a>
				</div>
				<!-- current -->
					<span style="position:absolute;color:#fff;font-size:15px;margin:17px;">您当前操作的餐厅为：<strong><?=$admin_restaurant?></strong></span>
				<!-- end current -->
				<!-- quick -->
				<?php if(isset($all_r)):?>
				<ul id="quick">
					<li>
						<a href="#" title="切换餐厅">
							<span class="icon">
								<img src="<?=WEB_ROOT?>resources/images/icons/application_double.png" alt="Products" />
							</span>
							<span>切换餐厅</span>
						</a>
						<ul>
						<?php foreach($all_r as $r):?>
							<li><a href="<?=WEB_ROOT?>admin/super_change_r/<?=$r['r_id']?>"><?=$r['r_name']?></a></li>
						<?php endforeach;?>
						</ul>
					</li>
				</ul>
				<?php endif;?>

				<div class="corner tl"></div>
				<div class="corner tr"></div>
			</div>
		</div>
		<!-- end header -->