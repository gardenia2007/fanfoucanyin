
<script type="text/javascript">
$(function(){

	$("button").button();

	$("tr").hover(function(){
		$(this).addClass("hover");
	},function(){
		$(this).removeClass("hover")
	}); 

	$("#loading").ajaxStart(function(){
		$(this).html("<img src='images/loading.gif'/><p>正在处理数据...</p>")
		.css('top',($(window).height() / 2 - 50))
		.css('left',($(window).width() / 2 - 100))
		.show();
	});

	$("button").click(function(){
		var content = $("#suggest textarea").val();
		if(content == ""){
			$("#loading").html("<img src='images/wrong.png'/><p>请输入建议内容</p>").show();
			setTimeout(function() {$("#loading").hide();}, 1200 );
			return;
		}

		$.post("<?=WEB_ROOT?>help/post_suggest",//ajax提交数据 
			{content:content},
				function(data){
					//alert(data);
					if(data == 0){
						$("#loading").html("<img src='<?=STATIC_ROOT?>images/success.png'/><p>建议提交成功！</p>");
						setTimeout(function() {$("#loading").hide();location.href = "<?=WEB_ROOT?>";}, 1200 );
					}else{
						$("#loading").html("<img src='<?=STATIC_ROOT?>images/error.png'/><p>"+data+"!</p>");
						setTimeout(function() {$("#loading").hide();}, 1200 );
					}
			});

	});

})
</script>
<style>
	#suggest_show table{font-size:12px;line-height:22px;}
	#next_back{font-size:13px;height:30px;position:relative;}
	#page_next{position:absolute;top:-10px;right:40px}
	#page_back{position:absolute;top:-10px;left:40px;}
	#suggest{width:500px;margin:25px auto 100px;}
	#suggest textarea{display:block;width:500px;border:1px solid #ccc;font-size:17px;padding:13px;resize:none;}
	#suggest button{margin-top:40px;margin-left:340px;}
	#suggest_tip{margin-top:40px;font-size:18px;width:640px;margin-left:50px;}
	#suggest_show tr{padding:30px auto;}


</style>
	
<div id='suggest_content'>
<p id='suggest_tip'>
欢迎您在此提出您的问题建议，对于合理的问题和建议，我们一定解决和采纳，以使饭否餐饮能更好地为您提供服务
</p>

<div id="suggest">
<textarea placeholder="请在此输入您的建议内容" rows='10'>
</textarea>
<button>提交建议</button>
</div>
</div>

<div id="suggest_show">
	
	<div>
	<table style="text-algin:center;width:s700px;margin:30px auto" class="ui-corner-all table">
		<tr class="table_header">
			<th width="25%">建议提交时间</th>
			<th width="75%">建议内容</th>
		</tr>
		<?php foreach($suggest as $i => $v):?>
		<tr>
			<td><?=$v->s_time?></td>
			<td><?=$v->s_content?></td>
		</tr>
		<?php endforeach;?>
	</table>
	</div>
</div>

<div class='r-show-pagination'>

<?php $page_show = ($current_page < 3)?1:($current_page - 2);$page_show--?>
<?php for($i = 1; $i <= $total_pages && $i <= 5; $i++):?>
<span class='page'<?php if($i == ($current_page + 1 )): echo "id='current-page'"?>><?=++$page_show?></span>
<?php else :?>n><a href='suggest?page=<?=$page_show?>'><?=++$page_show?></a></span><?php endif;?>
<?php endfor;?>

</div>
