<style>
#about_content {
	width: 710px;
	line-height: 32px;
	font-size: 15px;
	margin: 80px auto;
}
</style>
<div id="about_content">

<p>饭否餐饮成立于2012年1月，致力于提供优质的午餐和贴心的服务，满足IT人士的午餐需求。滋养你的胃。</p>

<p>我们的官方网站是<a href="http://fanfoufood.sinaapp.com">fanfoufood.sinaapp.com</a>，官方微博<a
	href="http://weibo.com/fanfoufood">weibo.com/fanfoufood</a>。</p>

<p>如有问题和建议,欢迎拨打咨询热线：18601381983，15810965838。</p>

</div>
