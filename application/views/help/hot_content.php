<style>
#hot-content {
	width: 710px;
	line-height: 32px;
	font-size: 15px;
	margin: 80px auto;
}
</style>
<div id="hot-content">

<div id="hot-r">
<h4>本周订餐数量排行榜</h4>
<table>
	<tr>
		<th width="20%">排名</th>
		<th width="59%">餐馆名称</th>
		<th width="20%">订餐数量</th>
	</tr>
<?php foreach($hot_r as $i => $v):?>
	<tr>
		<td><?=$i+1?></td>
		<td><a href="<?=$url?>show/restaurant?id=<?=$v['r_id']?>"><?=$v['r_name']?></a></td>
		<td><?=$v['r_order_num']?></td>
	</tr>
<?php endforeach;?>
</table>
</div>


<!-- 
<div id="hot-dish">

<table>
	<tr>
		<th>餐馆名称</th>
		<th>订餐数量</th>
	</tr>
<?php //foreach($hot_dish as $i => $v):?>
	<tr>
	</tr>
<?php //endforeach;?>
</table>
</div>
 -->


</div>
