<?php
class Index extends CI_Controller{


	var $siteurl = "";

	public function __construct()
	{
		parent::__construct();

		$this->siteurl = site_url();
		$this->load->model('index_model', '', TRUE);

		define('WEB_ROOT',$this->siteurl);
		define('STATIC_ROOT',WEB_ROOT.'resources/');
		define('EACH_PAGE_SHOW', 10);
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * 首页显示
	 */
	public function index()
	{
		$page = ($this->input->get("page") !== FALSE)?$this->input->get("page"):0;

		$data['r_info'] = $this->index_model->show_by_page($page);

		//var_dump($data);

		$data['title'] = '饭否餐饮';

		$data['total_pages'] = $this->index_model->how_many_pages();
		$data['current_page'] = $page;

		//var_dump($data['total_page']);
		$this->load->model('show_model','',TRUE);
		$data['r_hot'] = $this->show_model->hot_restaurant();

		$this->_show($data);

	}

	/**
	 * 根据标签选择情况，搜索餐厅
	 */
	public function search_tag()
	{
		//var_dump($_POST);
		$stag = $_POST['tag'];
		$stagc = $_POST['tag_c'];
		//var_dump($stagc);

		$tag = explode(',',$stag);
		$tagc = explode(',',$stagc);

		$page = isset($_GET['page'])?$_GET['page']:0;//痛苦的分页

		$data = $this->index_model->show_by_tag($tag, $tagc, $page);
	
		$data['current_page'] = $page;
		
		$this->load->view('index/content',$data);
		//echo "success!";
	}


	private function _show($data)
	{
		$data['url'] = $this->siteurl;
		//$this->load->view('common/include', $data);
		$this->load->view('index/header', $data);
		$this->load->view('common/nav', $data);
		$this->load->view('index/left_select_tag', $data);
		$this->load->view('index/content', $data);
		$this->load->view('index/right_info', $data);
		$this->load->view('common/footer', $data);

	}



}