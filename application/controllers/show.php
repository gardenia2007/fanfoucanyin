<?php
class Show extends CI_Controller{
	
	var $baseurl = "";
	
	public function __construct()
	{
		parent::__construct();
		
		$this->baseurl = base_url();
		$this->load->model('show_model', '', TRUE);
		
		define('WEB_ROOT',$this->baseurl);
		define('STATIC_ROOT',WEB_ROOT.'resources/');
		$this->output->enable_profiler(TRUE);
	}
	
	//查看餐厅
	public function restaurant()
	{
		//var_dump($_GET);

		$id = $this->input->get('id');
		$url = $this->input->get('url');
		
		if($url !== FALSE){
			if(!$this->show_model->check_url_exist($url))
				redirect('');
		}

		$data = $this->show_model->restaurant($id,$url);

		$data['title'] = '查看餐厅';
		
		$data['r_hot'] = $this->show_model->hot_restaurant();

		$this->_show($data);

	}
	
	
	private function _show($data)
	{
		$data['url'] = $this->baseurl;
		//$this->load->view('common/include', $data);
		$this->load->view('show/header', $data);
		$this->load->view('common/nav', $data);
		$this->load->view('show/content', $data);
		$this->load->view('show/right_info', $data);
		$this->load->view('common/footer', $data);
		
	}
	
	
	
	
}