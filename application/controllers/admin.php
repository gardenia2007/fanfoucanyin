<?php

class Admin extends CI_Controller
{

	/**
	 *  表单输入错误提示信息的显示框架
	 *
	 * @var string
	 * @access public
	 */
	var $form_wrong_msg_a = '<div class="messages"><div id="message-error" class="message message-error"><div class="image"><img src="';
	var $form_wrong_msg_b = '</span></div><div class="dismiss"><a href="#message-error"></a></div></div></div>';

	var $baseurl = "";

	var $restaurant;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('session');
		//变量初始化
		$this->form_wrong_msg_a = $this->form_wrong_msg_a.base_url().'resources/images/icons/error.png" alt="Error" height="32" />
			</div><div class="text"><h6>错误信息</h6><span>';
		$this->baseurl = base_url();
		$this->restaurant = $this->session->userdata('admin_restaurant');
		$this->load->model('admin_model','',TRUE);
		define('WEB_ROOT',$this->baseurl);
		define('STATIC_ROOT',WEB_ROOT.'resources/');
		$this->output->enable_profiler(TRUE);

	}


	public function login()
	{
		//print_r($this->session->all_userdata());

		if(!file_exists('application/views/admin/login.php'))
		{
			show_404();
		}

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username','用户名','trim|required');
		$this->form_validation->set_rules('password','密码','trim|required');
		$this->form_validation->set_error_delimiters($this->form_wrong_msg_a,$this->form_wrong_msg_b);

		if($this->form_validation->run() === TRUE){

			$r = $this->admin_model->check_login_password($this->input->post('username'),$this->input->post('password'));

			if($r !== FALSE)
			{
				$this->session->set_userdata('admin_name',$this->input->post('username'));
				$this->session->set_userdata('admin_power',$r->power);
				$this->session->set_userdata('admin_restaurant',$r->restaurant);
				$this->session->set_userdata('admin_restaurant_name',$r->r_name);
				//echo $r->r_name;

				//var_dump($this->session->all_userdata());
				$this->restanrant = $r->restaurant;

				redirect('admin/index');
			}else{
				$data['failed'] = TRUE;
			}
		}

		$data['title'] = '饭否餐饮-后台管理 登录';

		$this->load->view('admin/login',$data);

	}


	public function logout()
	{
		$item = array('admin_name'=>'','admin_power'=>'','admin_restaurant'=>'');
		$this->session->unset_userdata($item);
		redirect('');
	}

	//默认页面
	public function index()
	{
		//$this->output->cache(2);
		//$this->_check_auth_status();
		//if(!$this->session->userdata('adminname')) redirect('admin/login');

		$data['title'] = "饭否餐饮-后台管理";

		$date = date('Y-m-d');
		$date_array = explode('-',$date);
		$data['date'] = $date_array[0].'年'.$date_array[1].'月'.$date_array[2].'日';
		$data['order'] = $this->admin_model->show_order($this->restaurant,$date);
		//var_dump($data);
		$this->_view($data);
	}

	//显示订单
	public function show()
	{
		$date = $this->input->post('date');
		$date = $date ? $date : $this->uri->segment(3);
		//echo $date;
		if($date){
			$data['order'] = $this->admin_model->show_order($this->restaurant,$date);

			$data['title'] = '查看订单';
			$date_array = explode('-',$date);
			$data['date'] = $date_array[0].'年'.$date_array[1].'月'.$date_array[2].'日';
			$this->_view($data);
		}else{
			$data['title'] = '选择订单日期';
			$this->_view($data, 'show_order_select_date');
		}

	}


	//菜品
	public function add_dish($caller = TRUE)
	{
		//var_dump($_FILES);
		//var_dump($this->session->userdata('admin_restaurant'));
		$data['title'] = '添加菜品';

		$data['msg'] = FALSE;

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name','菜品名','required|min_length[2]');
		$this->form_validation->set_rules('type','菜品类别','required');
		$this->form_validation->set_rules('price','价格','required|max_length[6]');
		$this->form_validation->set_rules('descripe','描述','required|max_length[20]');
		$this->form_validation->set_error_delimiters($this->form_wrong_msg_a,$this->form_wrong_msg_b);

		// 如果提交了信息，则转到添加菜品功能
		if($this->form_validation->run()){

			//echo "bbb";
			$post['d_name'] = $this->input->post('name');
			$post['d_type'] = $this->input->post('type');
			$post['d_price'] = $this->input->post('price');
			$post['d_descripe'] = $this->input->post('descripe');

			$post['d_status'] = '0';
			$post['d_restaurant'] = $this->restaurant;
			//$post[''] = $this->input->post('price');
			$add_msg = $this->admin_model->add_dish($post);

			//var_dump($_FILES);
			if (isset($_FILES['pic']) && $_FILES['pic']['size'] != 0) {
				$id = $this->db->insert_id();
				$upload_msg = $this->upload_pic($id, 'dish');
			}else{
				$upload_msg = TRUE;
			}


			//$data['msg'] = ($upload_msg === TRUE)?TRUE:$upload_msg;
			$data['msg'] = ($add_msg === TRUE)?$upload_msg:$add_msg;

		}
		//显示页面
		//	var_dump($data);
		$data['act_success'] = ($data['msg'] === TRUE)?TRUE:FALSE;

		if($caller)
		$this->_view($data, array('all_dish_slide','add_dish'));
	}


	public function edit_dish($x = 0, $data = array())
	{
		if($this->uri->segment(3) !== FALSE){
			$data['id'] = $this->uri->segment(3);
		}

		if(isset($data['id'])){
			$id = $data['id'];
			$temp = $this->admin_model->get_info($id,'dish');
			//var_dump($data);
			$data = array_merge($data, $temp[0]);
		}

		$data['title'] = '修改菜品信息';

		$this->_view($data, array('all_dish_slide', 'edit_dish'));
	}

	/**
	 * 处理“修改菜品信息”页面提交的数据
	 */
	public function edit_dish_post()
	{
		$data['title'] = '修改菜品信息';
		$data['msg'] = FALSE;

		$id = $this->input->post("d_id");

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name','菜品名','required|min_length[2]');
		$this->form_validation->set_rules('type','菜品类别','required');
		$this->form_validation->set_rules('price','价格','required|max_length[6]');
		$this->form_validation->set_rules('descripe','描述','required|max_length[20]');
		$this->form_validation->set_error_delimiters($this->form_wrong_msg_a,$this->form_wrong_msg_b);

		// 如果提交了信息，则转到添加菜品功能
		if($this->form_validation->run() && $id !== FALSE){

			//echo "bbb";
			$post['d_name'] = $this->input->post('name');
			$post['d_type'] = $this->input->post('type');
			$post['d_price'] = $this->input->post('price');
			$post['d_descripe'] = $this->input->post('descripe');

			$post['d_status'] = '0';
			$post['d_restaurant'] = $this->restaurant;
			//$post['test'] = $this->input->post('pic');
			//var_dump($post);
			$add_msg = $this->admin_model->edit_info($id, $post, 'dish');

			// 如果用户提交了新的图片，则上传并更新之
			//var_dump($_FILES);
			if (isset($_FILES['pic']) && $_FILES['pic']['size'] != 0) {
				$upload_msg = $this->upload_pic($id, 'dish');
			}else{
				$upload_msg = TRUE;
			}
			//var_dump($add_msg);

			//$data['msg'] = ($add_msg === TRUE)?$upload_msg:$add_msg;
			$data['msg'] = ($upload_msg === TRUE)?TRUE:$upload_msg;
		}
		//echo $data['msg'];
		//显示页面
		$data['act_success'] = ($data['msg'] == TRUE)?TRUE:FALSE;
		$data['id'] = $id;

		//var_dump($data);
		$this->edit_dish(0,$data);
		//$this->_view($data, array('all_dish_slide', 'edit_dish'));
	}



	public function del_dish()
	{
		$data['title'] = '删除菜品';

		$a = $this->input->post('dish');
		//echo count($a);
		//var_dump($a);
		// 如果有输入
		if($a !== FALSE && count($a) > 0){
			$result = $this->admin_model->del_dish($a);
			//echo $result;
			if($result == count($a)){
				$data['act_success'] = TRUE;
			}else{
				$data['act_success'] = FALSE;
			}
		}

		$this->_view($data, array('all_dish_slide', 'del_dish'));
	}


	//添加，修改，删除类别
	public function add_type()
	{
		$data['title'] = '添加菜品分类';
		$data['msg'] = FALSE;
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name','名称','required');
		$this->form_validation->set_rules('descripe','描述','required');
		$this->form_validation->set_error_delimiters($this->form_wrong_msg_a,$this->form_wrong_msg_b);

		// 如果提交了信息，则转到添加菜品功能
		if($this->form_validation->run()){

			//echo "bbb";
			$post['t_name'] = $this->input->post('name');
			$post['t_descripe'] = $this->input->post('descripe');

			$post['t_status'] = '0';
			$post['t_restaurant'] = $this->restaurant;
			//$post[''] = $this->input->post('price');

			$data['msg'] = $this->admin_model->add_type($post);

		}

		$data['act_success'] = ($data['msg'] === TRUE)?TRUE:FALSE;

		$this->_view($data, array('all_dish_slide', 'add_type'));
	}


	public function edit_type()
	{
		$id = $this->uri->segment(3);
		if($id !== FALSE){
			$temp = $this->admin_model->get_info($id,'type');
			$data = $temp[0];
			$data['id'] = $id;
		}
		//var_dump($data);
		$data['title'] = '编辑菜品分类';

		$this->_view($data, array('all_dish_slide','edit_type'));
	}


	public function edit_type_post()
	{
		$data['title'] = '修改类别信息';


		$id = $this->input->post("id");

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name','名称','required');
		$this->form_validation->set_rules('descripe','描述','required');
		$this->form_validation->set_error_delimiters($this->form_wrong_msg_a,$this->form_wrong_msg_b);

		// 如果提交了信息，则转到添加菜品功能
		if($this->form_validation->run()){

			//echo "bbb";
			$post['t_name'] = $this->input->post('name');
			$post['t_descripe'] = $this->input->post('descripe');

			$post['t_status'] = '0';
			$post['t_restaurant'] = $this->restaurant;
			//$post[''] = $this->input->post('price');

			$data['msg'] = $this->admin_model->edit_info($id,$post,"type");

		}
		//显示页面

		$data['act_success'] = ($data['msg'] != 0)?TRUE:FALSE;

		$data['id'] = $id;

		$this->_view($data, array('all_dish_slide', 'edit_type'));
	}


	public function del_type()
	{
		$data['title'] = '删除菜品分类';

		$type = $this->input->post("type");

		if($type !== FALSE){
			$data['act_success'] = $this->admin_model->del_type($type);
		}

		$this->_view($data, array('all_dish_slide','del_type'));
	}


	//修改餐馆信息
	public function edit_r_info($data = array())
	{

		$temp = $this->admin_model->get_info($this->restaurant,'restaurant');

		$data = array_merge($data, $temp[0]);

		$data['title'] = '修改餐厅信息';

		$this->_view($data, 'edit_r_info');
	}


	public function edit_r_info_post()
	{
		$data['title'] = '修改餐厅信息';
		$data['msg'] = "";

		$id = $this->restaurant;
		$this->set_edit_r_info_form_rules();

		// 如果提交了信息，则转到添加菜品功能
		if($this->form_validation->run()){

			$post = $this->get_edit_r_info_post_data();

			$edit_msg = $this->admin_model->edit_info($id,$post,"restaurant");

			// 如果用户提交了新的图片，则上传并更新之
			if (isset($_FILES['pic']) && $_FILES['pic']['size'] != 0) {
				$upload_msg = $this->upload_pic($id, 'restaurant');
			}else{
				$upload_msg = TRUE;
			}

			//$data['msg'] = ($upload_msg === TRUE)?TRUE:$upload_msg;
			$data['msg'] = ($edit_msg === TRUE)?$upload_msg:$edit_msg;

		}
		//显示页面

		$data['act_success'] = ($data['msg'] === TRUE)?TRUE:FALSE;

		$this->edit_r_info($data);
	}


	public function edit_r_password()
	{
		$data['title'] = '修改登录密码';
		$data['msg'] = "";

		$this->load->library('form_validation');

		$this->form_validation->set_rules('old_password','旧密码','trim|required');
		$this->form_validation->set_rules('new_password','新密码','trim|required|matches[new_password2]');
		$this->form_validation->set_rules('new_password2','新密码','trim|required');
		$this->form_validation->set_error_delimiters($this->form_wrong_msg_a,$this->form_wrong_msg_b);

		if($this->form_validation->run()){
			$name = $this->session->userdata('admin_name');
			$password = $this->input->post('old_password');
			if($this->admin_model->check_login_password($name, $password) !== FALSE){
				$post = array();
				$post['password'] = $this->input->post('new_password2');
				$data['msg'] = $this->admin_model->edit_info($name,$post,"admin");
				$data['act_success'] = ($data['msg'] != 0)?TRUE:FALSE;
			}else{
				$data['act_success'] = FALSE;
				$data['msg'] = "原密码输入错误，请重新输入！";
			}
		}
		//显示页面
		$this->_view($data, 'edit_r_password');
	}


	/**
	 *	显示界面
	 *
	 * @param  array 需要向界面传输的数据
	 * @param  array(or string) 需要显示的部分
	 * @return void
	 */
	private function _view($data = array(),$show = 'show')
	{

		$this->load->library('form_validation');

		$data['url'] = $this->baseurl;
		$data['today'] = date('Y-m-d');
		$data['tomorrow'] = date('Y-m-d',time() + 86400);

		if(strcmp($show[0],'all_dish_slide') == 0){
			$data['all_type'] = $this->admin_model->get_all_type();
			$data['all_dish'] = $this->admin_model->get_all_dish();
		}

		if($this->session->userdata('admin_power') == 1){
			$data['all_r'] = $this->admin_model->get_restaurant();
		}
		$data['admin_restaurant'] = $this->session->userdata("admin_restaurant_name");

		$this->load->view('admin/header',$data);
		$this->load->view('admin/content-slide',$data);

		if(!is_array($show)){
			//var_dump($data);
			$this->load->view('admin/content/'.$show,$data);
		}
		else{
			foreach($show as $v){
				$this->load->view('admin/content/'.$v,$data);
			}
		}

		$this->load->view('admin/footer',$data);
	}


	function get_edit_r_info_post_data($f = TRUE){

		$post = array();
		if(!$f){
			$post['r_name'] = $this->input->post('name');
		}
		$post['r_service_time'] = $this->input->post('service_time');
		$post['r_type'] = $this->input->post('type');
		$post['r_url'] = $this->input->post('url');
		$post['r_delivery_price'] = $this->input->post('delivery_price');
		$post['r_delivery_min'] = $this->input->post('delivery_min');
		$post['r_delivery_range'] = $this->input->post('delivery_range');
		$post['r_delivery_cost_time'] = $this->input->post('delivery_cost_time');
		$post['r_delivery_start_time'] = $this->input->post('delivery_start_time');
		$post['r_delivery_stop_time'] = $this->input->post('delivery_stop_time');
		$post['r_phone'] = $this->input->post('phone');
		$post['r_address'] = $this->input->post('address');
		$post['r_descripe'] = $this->input->post('descripe');
		$post['r_special_dish'] = $this->input->post('special_dish');
		$post['r_activity'] = $this->input->post('activity');
		$post['r_status'] = $this->input->post('status');

		return $post;
	}


	function set_edit_r_info_form_rules($f = TRUE){

		$this->load->library('form_validation');
		if(!$f){
			$this->form_validation->set_rules('name','餐厅名称','required');
			$this->form_validation->set_rules('username','管理用户名','required');
			$this->form_validation->set_rules('name','管理密码','required');
		}
		$this->form_validation->set_rules('url','自定义域名','alpha_numeric|maxlength[32]');
		$this->form_validation->set_rules('service_time','营业时间','required');
		$this->form_validation->set_rules('type','类别','required');
		if($f){
			$this->form_validation->set_rules('delivery_price','送餐费用','required|numeric');
			$this->form_validation->set_rules('delivery_min','送餐最少订单金额','required|numeric');
			$this->form_validation->set_rules('delivery_range','送餐范围','required');
			$this->form_validation->set_rules('delivery_cost_time','送餐截止时间','required');
			$this->form_validation->set_rules('delivery_start_time','送餐开始时间','required');
			$this->form_validation->set_rules('delivery_stop_time','送餐截止时间','required');
			$this->form_validation->set_rules('address','地址','required');
			$this->form_validation->set_rules('descripe','餐厅描述','required');
		}
		$this->form_validation->set_rules('phone','联系电话','required|integer|min_length[7]');
		$this->form_validation->set_error_delimiters($this->form_wrong_msg_a,$this->form_wrong_msg_b);

	}



	/**
	 * 超级管理员更改管理的餐馆
	 */
	function super_change_r(){
		$id = $this->uri->segment(3);
		if($this->session->userdata('admin_power') == 1 && $id !== FALSE){
			$temp = $this->admin_model->get_restaurant($id, "one");
			$this->session->set_userdata('admin_restaurant',$id);
			$this->session->set_userdata('admin_restaurant_name',$temp[0]['r_name']);
			//echo $r->r_name;
			$this->restanrant = $id;

			redirect('admin/index');
		}
	}


	function add_r(){
		if($this->session->userdata('admin_power') == 1 ){
			$data['title'] = '添加餐厅';

			$this->set_edit_r_info_form_rules(FALSE);

			// 如果提交了信息，则转到添加菜品功能
			if($this->form_validation->run()){

				$post = $this->get_edit_r_info_post_data(FALSE);
				
				$user = array();
				$user['username'] = $this->input->post('username');
				$user['password'] = $this->input->post('passwd');

				$edit_msg = $this->admin_model->add_r($post,$user);

				// 如果用户提交了新的图片，则上传并更新之
				if (isset($_FILES['pic']) && $_FILES['pic']['size'] != 0) {
					$id = $this->db->insert_id();
					$upload_msg = $this->upload_pic($id, 'restaurant');
				}else{
					$upload_msg = TRUE;
				}

				$data['msg'] = ($upload_msg === TRUE)?TRUE:$upload_msg;
				$data['act_success'] = ($data['msg'] == TRUE)?TRUE:FALSE;
			}
			//显示页面

			$this->_view($data, 'add_r');

		}else{
			exit("对不起，您无此项权限！");
		}
	}


	/**
	 * 上传文件，并根据id和table的值更新数据库中的链接
	 * @param $id
	 * @param $table
	 */
	function upload_pic($id,$table){
		// 如果在SAE上运行，使用SAE_Storage
		if($this->config->item('sae')){
			$result = $this->sae_upload($id,$table);
		}
		// 如果不是在SAE上运行或者是在本地测试，使用PHP自带的上传机制
		else{
			$result = $this->local_upload($id,$table);
		}
		return $result;
	}


	/**
	 * 使用SAE Storage上传存储文件
	 * @param $id 图片所属记录的id
	 * @param $type 图片所属记录的表名
	 */
	function sae_upload($id,$table){
		//图片处理，调整大小，加水印
		$pic_name = md5(time()).".jpg";
		$temp_name = $_FILES['pic']['tmp_name'];
		$img_src = file_get_contents($temp_name);
		$img = new SaeImage($img_src);
		$img->annotate("饭否餐饮",0.5,SAE_SouthEast,
		array("name"=>SAE_SimSun, "size"=>15, "weight"=>300, "color"=>"black"));

		//存储
		$s = new SaeStorage();
		$url = $s->write($this->config->item['SaeStorage_pic'], $pic_name, $img->exec('jpg',false));

		//url存入数据库
		$result = $this->upload_save_database($id, $table,$url);
		if($result != 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}



	/**
	 * 原生的PHP上传
	 * @param unknown_type $id
	 * @param unknown_type $type
	 */
	function local_upload($id,$table){
		$config['upload_path'] = 'resources/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '2048';
		$config['encrypt_name']  = TRUE;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('pic'))
		{
			//返回错误信息
			echo $error = array('error' => $this->upload->display_errors());
			return $this->upload->display_errors();

		}else{
			$data = $this->upload->data();
			//var_dump($data);
			$url = $this->baseurl."resources/uploads/".$data["file_name"];

			//url存入数据库
			$result = $this->upload_save_database($id, $table,$url);
			if($result != 0){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}




	/**
	 * 把上传后的文件URL信息存入数据库
	 * @param $id
	 * @param $tabel
	 * @param $url
	 */
	function upload_save_database($id, $table, $url){
		$key = substr($table,0,1);
		$key .= "_logo_url";
		//echo $url;
		$data[$key] = $url;
		//echo $url;
		//更新数据库
		return $this->admin_model->edit_info($id, $data, $table);
	}


}



