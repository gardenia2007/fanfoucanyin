<?php

class Help extends CI_Controller
{

	var $baseurl = "";

	/**
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		$this->baseurl = base_url();
		$this->load->model('help_model', '', TRUE);

		define('WEB_ROOT',$this->baseurl);
		define('STATIC_ROOT',WEB_ROOT.'resources/');
		define('EACH_PAGE_SHOW', 10);
	}
	
	/**
	 * 发送信息
	 */
	public function send_sms(){
		$this->load->library('sms');
		$this->sms->send();
	}
	
	/**
	 * 发表建议
	 */
	public function suggest()
	{
		$data = array();
		$data['title'] = "发表建议";

		$page = ($this->input->get("page") !== FALSE)?$this->input->get("page"):0;

		$data['suggest'] = $this->help_model->show_suggest_by_page($page);

		//var_dump($data);

		$data['title'] = '饭否餐饮';

		$data['total_pages'] = $this->help_model->how_many_suggest_pages();
		$data['current_page'] = $page;
		
		$data['url'] = $this->baseurl;
		$this->load->view('help/header', $data);
		$this->load->view('common/nav', $data);
		$this->load->view('help/suggest_content', $data);
		$this->load->view('common/footer', $data);
		
	}
	
	
	public function post_suggest()
	{
		$content = $this->input->post('content');
		if(strlen($content) != 0){
			$result = $this->help_model->post_suggest($content);
			if($result == 1){
				echo "0";
			}else{
				echo "建议提交失败，请稍候再试";
			}
		}else{
			echo "请输入建议内容";
		}
	}
	
	
	/**
	 * 关于我们
	 */
	public function about_us()
	{
		$data = array();
		
		$data['title'] = "关于我们";
		
		$data['url'] = $this->baseurl;
		$this->load->view('help/header', $data);
		$this->load->view('common/nav', $data);
		$this->load->view('help/about_us_content', $data);
		$this->load->view('common/footer', $data);
		
	}
	
	
	/**
	 * 排行榜
	 */
	public function hot()
	{
		$data = array();
		
		$this->load->model('show_model', '', TRUE);
		$data['hot_r'] = $this->show_model->hot_restaurant(10);
		
		$data['title'] = "订餐排行榜";
		
		$data['url'] = $this->baseurl;
		$this->load->view('help/header', $data);
		$this->load->view('common/nav', $data);
		$this->load->view('help/hot_content', $data);
		$this->load->view('common/footer', $data);
	}



}