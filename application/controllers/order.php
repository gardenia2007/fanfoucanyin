<?php

class Order extends CI_Controller
{

	var $baseurl = "";

	public function __construct()
	{
		parent::__construct();

		$this->baseurl = base_url();
		$this->load->model('order_model', '', TRUE);

		define('WEB_ROOT',$this->baseurl);
		define('STATIC_ROOT',WEB_ROOT.'resources/');
	}
	/**
	 * 提交订单
	 */
	public function submit()
	{
		$data = $this->order_model->submit();
		
		echo $data;
		
		return;
	}

	/**
	 * 查看订单
	 */
	public function show()
	{
		$this->output->enable_profiler(TRUE);
		// 得到订单号
		if($this->session->flashdata('submit') === TRUE){
			// 如果用户刚提交完订单
			$sn = $this->session->flashdata('sn');
		}else{
			$sn = $this->input->post('sn', TRUE);
			//$sn = $this->input->
		}


		// 如果得到了订单号，显示订单信息
		if($sn){
			// 去掉用户输入的订单号中可能有的所有空格
			$sn = str_replace(' ','', $sn);
			$data = $this->order_model->show($sn);
			// 如果存在订单数据
			if($data !== FALSE){
				$data['title'] = '查看订单';
				$this->_view($data);
				return;
			}
			$data['sn'] = $sn;
			// 根据订单号没有找到订单信息
			$data['msg'] = TRUE;
		}else{
			// 没有输入订单号
			$data['msg'] = FALSE;
		}
		// 提示错误信息
		// 显示输入订单号页面
		$data['title'] = "查询订单";
		$this->_view($data, 'query');
	}


	
	/**
	 * 输入订单信息，查询订单
	 */
	public function query()
	{
		$this->output->enable_profiler(TRUE);
		$data['title'] = "查询订单";
		$this->_view($data, 'query');
	}


	/**
	 *	显示界面
	 *
	 * @param  array 需要向界面传输的数据
	 * @param  string 显示还是查询呢？
	 */
	private function _view($data = array(), $content = 'show')
	{
		$data['url'] = $this->baseurl;

		$this->load->view('order/header',$data);
		$this->load->view('common/nav',$data);
		$this->load->view('order/'.$content.'_content',$data);
		//	$this->load->view('order/',$data);
		$this->load->view('common/footer',$data);
	}



}