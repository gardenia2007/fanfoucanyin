<?php
/**
 * * 后台权限拦截钩子 
 * @link http://www.php-chongqing.com
 * @author bing.peng
 *
 **/
class AdminAuth {
	private $CI;
	public function __construct()
	{
		$this->CI = &get_instance();
	}
	/**
	 * 权限认证 
	 */
	public function auth()
	{
		/**
echo  "<br/>".$this->CI->uri->segment(1);
echo  "<br/>".$this->CI->uri->segment(2);
**/
		if ( $this->CI->uri->segment(1) == 'admin' && $this->CI->uri->segment(2) != 'login') {
			// 需要进行权限检查的URL
			$this->CI->load->library('session');
			//echo "<br/>".$this->CI->session->userdata('admin_name');
			if( !$this->CI->session->userdata('admin_name') ) {
				// 用户未登陆 
			//	echo  "<br/>"."hook!!! <br/> <br/>";
			//	var_dump($this->CI->session->all_userdata());
				redirect('admin/login');
				return;
			}
			//echo " <br/>not_hook!!!";
		}
	}
}