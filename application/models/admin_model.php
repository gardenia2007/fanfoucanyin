<?php

class Admin_model extends CI_Model
{

	var $restaurant;

	public function __construct()
	{
		parent::__construct();

		$this->restaurant = $this->session->userdata('admin_restaurant');
	}



	/**
	 * 得到登录信息
	 * @param unknown_type $name
	 * @param unknown_type $password
	 * @param $m = 0 验证密码 / $m = 1 得到登录信
	 */
	function check_login_password($name, $password,$m = 0)
	{
		$sql = "SELECT `restaurant`,`power`,`r_name` FROM `admin`,`restaurant` WHERE `r_id` = `restaurant` AND `username` = '{$name}'";
		if($m == 0){
			$sql = $sql." AND `password` = '{$password}'";
		}
		$sql .= " AND `status` = '0';";
		$q = $this->db->query($sql);
		if($q->num_rows() != 0){
			$temp = $q->result_object();
			return $temp[0];
		}else{
			return FALSE;
		}
	}

	/**
	 * 显示所有的订单
	 * @param $restaurant
	 * @param $date
	 */
	function show_order($restaurant, $date){

		$date = $this->db->escape($date);
		$restaurant = $this->db->escape($restaurant);

		$sql = "SELECT `id`, `sn`, `phone`, `address`,`building`, `room`, `status`, `time`
					FROM `order` WHERE `date` = {$date} AND 
					EXISTS
					(
						SELECT * FROM `order_detail` 
						WHERE order_detail.r_id = {$restaurant} AND order_detail.o_id = order.id
					)
					ORDER BY `time` DESC";


		//print_r($this->session->all_userdata());
		$query = $this->db->query($sql);

		//var_dump($query->result_array());
		$result = array();
		$result = $query->result_array();

		foreach($result as $key => $v){
			//找出指定的订单下属于该餐厅的菜品信息
			$sql = "SELECT dish.d_name, dish.d_price, order_detail.d_num
						FROM `dish`, `order_detail` 
						WHERE dish.d_id = order_detail.d_id 
						AND order_detail.o_id = {$v['id']} 
						AND order_detail.r_id = {$restaurant};";

			$query = $this->db->query($sql);

			$result[$key]['detail'] = $query->result_array();

			//找到当前订单属于该餐厅的金额
			$sql = "SELECT `price` FROM `order_restaurant`
					WHERE `o_id` = {$v['id']} AND `r_id` = {$restaurant};";
			$query = $this->db->query($sql);
			$temp = $query->result_array();
			$result[$key]['price'] = $temp[0]['price'];

		}

		//var_dump($result);

		return $result;
	}


	/**
	 * 得到指定餐馆的所有的菜品类别
	 * @param $restaurant 餐馆的ID
	 */
	function get_all_type(){

		$sql = "SELECT `t_id`,`t_name` FROM `type` WHERE `t_status` = '0' AND `t_restaurant` = {$this->restaurant};";

		$q = $this->db->query($sql);

		return $q->result_array();

	}

	/**
	 * 按分类得到所有的菜品
	 */
	function get_all_dish()
	{
		$data = array();

		$type = $this->get_all_type();

		foreach($type as $i => $v){
			$sql = "SELECT `d_id`, `d_name` FROM `dish` WHERE `d_type` = {$v['t_id']} AND `d_status` = '0';";
			$q = $this->db->query($sql);
			$data[$i] = $q->result_array();
		}

		return $data;

	}

	/**
	 * 插入菜品，返回信息
	 * @param unknown_type $input
	 */
	function add_dish($input)
	{
		$sql = "SELECT * FROM `dish` WHERE `d_name` = '{$input['d_name']}'
				AND `d_type` = '{$input['d_type']}' AND `d_restaurant` = {$this->restaurant}";
		$q = $this->db->query($sql);
		if($q->num_rows() != 0){
			return "当前已存在相同信息的菜品！";
		}
		if($this->db->insert('dish', $input)){
			return TRUE;
		}else{
			return "插入失败，请稍候再试！";
		}
	}

	function del_dish($id)
	{
		$result = 0;
		foreach($id as $v){
			$sql = "UPDATE `dish` SET `d_status` = '1' WHERE `d_id` = {$v};";
			$this->db->query($sql);
			$result += $this->db->affected_rows();
		}
		return $result;
	}

	/**
	 * 根据表名生成SQL语句中的"(WHERE) id = xx"部分
	 * @param unknown_type $id
	 * @param unknown_type $table
	 */
	private function get_where_condition($id , $table){
		//如果要更改dish表
		if(strcmp($table,"dish") == 0){
			$where = "d_id = {$id}";
		}
		//如果要更改type表
		else if(strcmp($table,"type") == 0){
			$where = "t_id = {$id}";
		}
		//如果要更改restaurant表
		else if(strcmp($table,"restaurant") == 0){
			$where = "r_id = {$id}";
		}
		//如果要更改admin表
		else if(strcmp($table,"admin") == 0){
			$where = "username = '{$id}'";
		}

		return $where;
	}

	/**
	 * 修改 菜品/类别/餐厅 信息
	 * @param $id
	 * @param $data 新的数据
	 * @param $table 表名
	 */
	function edit_info($id,$data,$table)
	{
		if($table == 'restaurant'){
			// 自定义域名不可重名
			$sql = "SELECT * FROM `restaurant` WHERE `r_url` = '{$data['r_url']}';";
			$q = $this->db->query($sql);
			if($q->num_rows() != 0){
				return "当前已存在自定义域名'{$data['r_url']}'，请重新进行选择！";
			}
		}
		//var_dump($data);
		$where = $this->get_where_condition($id,$table);
		// 执行更新操作
		$this->db->update($table, $data,$where);
		if($this->db->affected_rows() != 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}


	/**
	 * 得到 菜品/类别/餐厅 信息
	 * @param $id
	 * @param $table 表名
	 */
	function get_info($id,$table)
	{
		if($id !== FALSE)
		{
			$where = $this->get_where_condition($id, $table);
			$sql = "SELECT * FROM `{$table}` WHERE {$where};";
			$q = $this->db->query($sql);
			return $q->result_array();
		}
		else
		return array();
	}

	/**
	 * 插入类别，返回操作结果
	 * @param unknown_type $input
	 */
	function add_type($input)
	{
		$sql = "SELECT * FROM `type` WHERE `t_name` = '{$input['t_name']}'
				AND `t_restaurant` = {$this->restaurant}";
		$q = $this->db->query($sql);
		if($q->num_rows() != 0)
		return "当前已存在相同信息的菜品！";
		if($this->db->insert('type', $input))
		return TRUE;
		else
		return "插入失败，请稍候再试！";
	}


	function del_type($id)
	{
		$sql = "UPDATE `type` SET `t_status` = '1' WHERE `t_id` = {$id};";
		$this->db->query($sql);
		$this->db->affected_rows();
		if($this->db->affected_rows() != 0){
			return TRUE;
		}
	}


	function get_restaurant($id = 0, $m = 'all')
	{
		$sql = "SELECT `r_id`, `r_name` FROM `restaurant`";
		if($m == 'one'){
			$sql = $sql." WHERE `r_id` = {$id};";
		}

		$q = $this->db->query($sql);
		return $q->result_array();
	}


	/**
	 * 插入菜品，返回信息
	 * @param array $input 餐厅的基本信息
	 * @param array $user 餐厅的管理用户名和密码
	 */
	function add_r($input,$user)
	{
		//添加餐馆的条件限制为不能有重名
		$sql = "SELECT * FROM `restaurant` WHERE `r_name` = '{$input['r_name']}';";
		$q = $this->db->query($sql);
		if($q->num_rows() != 0){
			return "当前已存在同名的餐馆！";
		}
		// 自定义域名不可重名
		$sql = "SELECT * FROM `restaurant` WHERE `r_url` = '{$input['r_url']}';";
		$q = $this->db->query($sql);
		if($q->num_rows() != 0){
			return "当前已存在自定义域名'{$input['r_url']}'，请重新进行选择！";
		}
		//管理用户名也不能重复
		$sql = "SELECT * FROM `admin` WHERE `username` = '{$user['username']}';";
		$q = $this->db->query($sql);
		if($q->num_rows() != 0){
			return "当前已存在相同的管理用户名！";
		}
		// 如果以上条件都已满足，则执行插入操作
		if($this->db->insert('restaurant', $input)){
			$user['restaurant'] = $this->db->insert_id();
			if($this->db->insert('admin', $user)){
				return TRUE;
			}
		}
		return "插入失败，请稍候再试！";
	}


}