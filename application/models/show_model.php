<?php

class Show_model extends CI_Model
{
	var $sql = "";
	

	public function __construct()
	{
		parent::__construct();
		define('HOT_LIST_NUM', 5);
	}

	public function _query()
	{
		return $this->db->query($this->sql);
	}
	
	public function check_url_exist($url){
		$this->sql = "SELECT * FROM `restaurant` WHERE `r_url` = '{$url}' LIMIT 0,1;";
		$query = $this->_query();
		return ($query->num_rows() != 0);
	}
	
	public function restaurant($id, $url = FALSE)
	{

		$data = array();

		//如果没有设置域名
		if($url === FALSE)
			$this->sql = "SELECT * FROM `restaurant` WHERE `r_id` = {$id} LIMIT 0,1;";
		else
			$this->sql = "SELECT * FROM `restaurant` WHERE `r_url` = '{$url}' LIMIT 0,1;";

		$query = $this->_query();

		$data['r_info'] = $query->row_array();

		$id = $data['r_info']['r_id'];

		//选出属于该餐厅的所有类别
		$this->sql = "SELECT `t_name`,`t_id` FROM `type`
						WHERE `t_restaurant` = '{$id}'  
						AND `t_status` = '0' ORDER BY `t_id` ASC;";
		//echo $this->sql;
		$q = $this->_query();
		//var_dump($all_type);
		$all_type = $q->result_array();

		$data['all'] = array();
		$data['dish_has_logo'] = array();

		foreach($all_type as $key => $type)
		{
			// 本类别菜品是否存在带有图片的菜品
			$this->sql = "SELECT * FROM `dish` WHERE `d_restaurant` = {$id} AND `d_status` = '0'
							AND `d_type` = '{$type['t_id']}' AND `d_logo_url` <> 'NULL' ";
			//echo $this->sql;
			$q = $this->_query();
			$data['dish_has_logo'][$type['t_name']] = ($q->num_rows() != 0)?TRUE:FALSE;

			// 本类别菜品是否存在不带有图片的菜品
			$this->sql = "SELECT * FROM `dish` WHERE `d_restaurant` = {$id} AND `d_status` = '0'
							AND `d_type` = '{$type['t_id']}' AND `d_logo_url` = 'NULL' ";
			$q = $this->_query();
			$data['dish_no_logo'][$type['t_name']] = ($q->num_rows() != 0)?TRUE:FALSE;
			//echo $this->sql;
			$this->sql = "SELECT `d_id`,`d_name`,`d_price`,`d_logo_url`,`d_descripe` FROM `dish`
							WHERE `d_restaurant` = '{$id}' AND `d_type` = '{$type['t_id']}' AND `d_status` = '0';";
			$q = $this->_query();
			$data['all'][$type['t_name']] = $q->result_array();
		}

		return $data;
	}

	
	public function hot_restaurant($num_of_result = 5,$days = 7)
	{
		$today = date("Y-m-d");
		$today_timestamp = time();
		do{
			$another_day_timestamp = $today_timestamp - $days * 24 * 60 * 60;
			$another_day = date("Y-m-d", $another_day_timestamp);
			$date_condition = "`date` <= '{$today}' AND `date` >= '{$another_day}'";
			$this->sql = "SELECT order_detail.r_id, r_name, COUNT( * ) AS r_order_num
						FROM  `order` ,  `order_detail` ,  `restaurant` 
						WHERE restaurant.r_id = order_detail.r_id AND o_id = id 
						AND {$date_condition} GROUP BY r_id  
						ORDER BY r_order_num DESC LIMIT 0, {$num_of_result};";
			$q = $this->_query();
			//增加时间范围，以达到需要的结果数
			$days += 2;
		}
		// 当结果达到需要的结果数时才结束
		while($q->num_rows() < HOT_LIST_NUM);
		
		return $q->result_array();
	}


}