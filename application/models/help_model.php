<?php

class Help_model extends CI_Model
{

	var $sql = "";

	public function _query()
	{
		return $this->db->query($this->sql);
	}
	
	public function post_suggest($content)
	{
		$this->sql = "INSERT INTO `suggestion` (`s_content`, `s_status`) VALUES ('{$content}', '0');";
		$this->_query();
		return $this->db->affected_rows();
	}
	
	public function show_suggest_by_page($page)
	{
		//$res = $this->query();
		$limit = $page * EACH_PAGE_SHOW;
		$each_page_show = EACH_PAGE_SHOW;
		$this->sql = "SELECT * FROM `suggestion` WHERE `s_status` = '0' ORDER BY `s_time` DESC LIMIT {$limit}, {$each_page_show};";
		//echo $this->sql;

		$q = $this->_query();
		return $q->result();

	}


	public function how_many_suggest_pages()
	{
		$this->sql = "SELECT * FROM `suggestion` WHERE `s_status` = '0'";
		$q = $this->_query();
		$total_rows = $q->num_rows();
		$total_page = ceil($total_rows / EACH_PAGE_SHOW);

		return $total_page;
	}
}