<?php

class Index_model extends CI_Model
{

	var $sql = "";

	public function _query()
	{
		return $this->db->query($this->sql);
	}
	
	
	public function show_by_page($page)
	{
		//$res = $this->query();
		$limit = $page * EACH_PAGE_SHOW;
		$each_page_show = EACH_PAGE_SHOW;
		$this->sql = "SELECT * FROM `restaurant` WHERE 1 ORDER BY `r_id` ASC LIMIT {$limit}, {$each_page_show};";
		//echo $this->sql;

		$q = $this->_query();
		return $q->result_array();

	}

	public function show_by_tag($tag, $tagc, $page)
	{
		$this-> sql = 'SELECT * FROM `restaurant` WHERE 1 ';
		foreach($tag as $i => $v)
		{
			if($tagc[$i] == '2')
			{
				if($v == "30元以上")
				$this->sql .= "AND `r_delivery_min` > 30 ";
				else
				{
					$temp = str_split($v, 2);
					$this->sql .= "AND `r_delivery_min` < {$temp[0]} ";
				}
			}
			else if($tagc[$i] == '1')
			{
				if($v == "免送餐费")
				$this->sql .= "AND `r_delivery_price` = 0 ";
				if($v == "收取送餐费")
				$this->sql .= "AND `r_delivery_price` <> 0 ";
			}
			else if($v != '')
			{
				$this->sql .= "AND `r_type` LIKE '%{$v}%' ";
			}
		}
		//echo $this->sql;
		$q = $this->_query();
		
		$data = array();
		$data['r_info'] = $q->result_array();
		
		$total_rows = $q->num_rows();
		$data['total_pages'] =  ceil($total_rows / EACH_PAGE_SHOW);
		return $data;
	}


	public function how_many_pages()
	{
		$total_rows = $this->db->count_all('restaurant');

		$total_page = ceil($total_rows / EACH_PAGE_SHOW);

		return $total_page;
	}
}