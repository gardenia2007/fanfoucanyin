<?php

class Order_model extends CI_Model
{
	var $sql = "";

	public function _query()
	{
		return $this->db->query($this->sql);
	}

	/**
	 * 处理提交订单
	 */
	public function submit()
	{
		$current_time = time();

		$sn = $this->generate_sn($current_time);
		//得到所有经过安全过滤POST数据
		$post = $this->input->post(NULL , TRUE);
		//var_dump($post);
		$data = array(
			'sn' => $sn,
			'phone' => $post['info']['phone'],
			'address' => $post['info']['address'],
			'building' => $post['info']['building'],
			'room' => $post['info']['room'],
			'date' => date("Y-m-d", $current_time),
			'time' => date("H:i:s", $current_time)
		);
		//var_dump($data);
		$status = $this->db->insert('order', $data);
		if(!$status){
			//$this->submit_rollback($o_id);
			return "first insert failed!";
		}

		$order_id = $this->db->insert_id();

		// 把订单详细信息存入数据库
		$total = 0;
		$order_r_price = array();
		foreach($post as $r_id => $r_value){
			if(isset($r_value["address"])) continue;
			$r_total = 0 ;
			foreach($r_value as $id => $value){

				if($id == "info" || $value['count'] <= 0)
				continue;

				$data = array(
				'o_id' => $order_id,
				'd_id' => $id,
				'd_num' => $value['count'],
				'r_id' => $r_id
				);
				$status = $this->db->insert('order_detail', $data);

				if(!$status){
					$this->submit_rollback($order_id);
					return "order_detail failed!!";
				}
				//计算每家餐馆总金额
				$r_total += $value['count'] * $value['price'];

			}
			// 将餐厅的配送费存入数据库
			$data = array(
				'o_id' => $order_id,
				'r_id' => $r_id,
				'price' => $r_value['info']['price']
			);
			$status = $this->db->insert('order_delivery', $data);
			if(!$status){
				$this->submit_rollback($order_id);
				return "error";
			}
			$r_total += $data['price'];
			//将订单分属于各餐厅的订单价格存入数据库
			$data = array(
				'o_id' => $order_id,
				'r_id' => $r_id,
				'price' => $r_total
			);
			//array_push($order_r_price,$data);
			$status = $this->db->insert('order_restaurant', $data);
			if(!$status){
				$this->submit_rollback($order_id);
				return "error";
			}
			
			//var_dump($r_value);
			

			$total += $r_total;

		}

		// 更新订单总价格
		$this->sql = "UPDATE `order` SET `price` = '{$total}' WHERE `id` = '{$order_id}';";
		//echo $this->sql;
		$this->db->query($this->sql);

		if($this->db->affected_rows() == 0){
			$this->submit_rollback($order_id);
			return "update error";
		}

		//处理成功
		$this->session->set_flashdata('submit', TRUE);
		$this->session->set_flashdata('sn', $sn);

		$this->load->library('sms');

		//向商家发送信息
		$this->sms->order_sms($order_r_price);

		return "0";

	}

	/**
	 * 订单添加失败后的回滚操作
	 * @param $id
	 */
	private function submit_rollback($id) {
		$this->sql = "DELETE * FROM `order` WHERE `id` = {$id};";
		$this->db->query($this->sql);
		$this->sql = "DELETE * FROM `order_detail` WHERE `o_id` = {$id};";
		$this->db->query($this->sql);
		$this->sql = "DELETE * FROM `order_restaurant` WHERE `o_id` = {$id};";
		$this->db->query($this->sql);
		$this->sql = "DELETE * FROM `order_delivery` WHERE `o_id` = {$id};";
		$this->db->query($this->sql);
	}


	/**
	 * 根据订单号，返回订单详细信息
	 * @param  $sn
	 */
	public function show($sn)
	{

		$sn = trim($sn);

		$data = array();

		$data['sn_show'] = str_split($sn, 4);

		$this->sql = "SELECT id,address,building,room,phone,date,time FROM `order` WHERE `sn` = '{$sn}';";

		$q = $this->_query();

		if($q->num_rows() != 0){
			// 送餐信息
			$data['order_info'] = $q->row_array();
			
			$o_id = $data['order_info']['id'];

			$this->sql = "SELECT dish.d_name,restaurant.r_name,dish.d_price,order_detail.d_num,order.price
					FROM `restaurant`,`dish`,`order_detail`,`order`
					WHERE restaurant.r_id = dish.d_restaurant 
					AND dish.d_id = order_detail.d_id 
					AND order_detail.o_id = order.id 
					AND order.id = '{$o_id}'
					LIMIT 0, 30 ;";

			$q = $this->_query();

			$data['dish'] = $q->result_array();
			//var_dump($data);
			
			$this->sql = "SELECT r.r_name, d.price 
			FROM `restaurant` AS r, `order_delivery` AS d
					WHERE r.r_id = d.r_id AND d.o_id = {$o_id};";
			
			$q = $this->_query();

			$data['delivery'] = $q->result_array();
			
			//订单总价格
			$data['total_price'] = $data['dish'][0]['price'];

			return $data;
		}else{
			return FALSE;
		}
	}




	/**
	 * 根据订单号和餐厅id，得到发送手机信息时需要的信息
	 */
	public function get_sms_info($order_id, $r_id) {

		$ret = array();
		$ret['customer'] = array();
		$ret['dish'] = array();

		// 得到餐厅的联系电话
		$this->sql = "SELECT `r_phone` FROM `restaurant` WHERE `r_id` = {$r_id};";
		$q = $this->_query();
		if($q->num_rows() > 0){
			$temp = $q->row_array();
			$ret['r_phone'] = $temp['r_phone'];
		}else{
			return FALSE;
		}

		// 得到用户下订单时填写的电话号码，地址和下单时间
		$this->sql = "SELECT `phone`,`address`,`building`, `room`, `time` FROM `order` WHERE `id` = {$order_id};";
		$q = $this->_query();
		if($q->num_rows() != 0){
			$temp = $q->row_array();
			$ret['customer']['phone'] = $temp['phone'];
			$ret['customer']['address'] = $temp['address'];
			$ret['customer']['building'] = $temp['building'];
			$ret['customer']['room'] = $temp['room'];
			$ret['customer']['time'] = $temp['time'];
		}else{
			return FALSE;
		}

		// 得到订单菜品信息
		$this->sql = "SELECT c.d_name, b.d_num FROM `order_restaurant` AS a, `order_detail` AS b, `dish` AS c
					WHERE c.d_id = b.d_id AND b.o_id = a.order_id AND b.r_id = a.r_id 
						AND a.order_id = {$order_id} AND a.r_id = {$r_id}";

		$q = $this->_query();
		if ($q->num_rows() > 0)
		{
			$t = $q->result_array();
			foreach ($t as $key => $row)
			{
				$ret['dish'][$key + 1]['name'] = $row['d_name'];
				$ret['dish'][$key +1 ]['num'] = $row['d_num'];
			}
		}else{
			return FALSE;
		}

		return $ret;
	}



	/**
	 * 生成订单号
	 * @param $time
	 */
	private function generate_sn($time)
	{
		$year_code = array('A','B','C','D','E','F','G','H','I','J');
		$sn = $year_code[intval(date('Y'))-2012].strtoupper(dechex(date('m'))).date('d'). substr($time,-5).substr(microtime(),2,5).rand(0,9);//生成订单号
		return $sn;
	}


}