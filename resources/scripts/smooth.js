/* path to the stylesheets for the color picker */
var style_path = "resources/css/colors";

$(document).ready(
		function() {
			/* messages fade away when dismiss is clicked */
			$(".message > .dismiss > a").live("click", function(event) {
				var value = $(this).attr("href");
				var id = value.substring(value.indexOf('#') + 1);

				$("#" + id).fadeOut('slow', function() {
				});

				return false;
			});

			/* color picker */
			$("#colors-switcher > a")
					.click(
							function() {
								var style = $("#color");

								style.attr("href", "" + style_path + "/"
										+ $(this).attr("title").toLowerCase()
										+ ".css");

								return false;
							});

			$("#menu h6 a").click(function(event) {
				event.preventDefault();
				var link = $(this);
				var value = link.attr("href");
				var id = value.substring(value.indexOf('#') + 1);

				var heading = $("#h-menu-" + id);
				var list = $("#menu-" + id);

				if (list.attr("class") == "closed") {
					heading.attr("class", "selected");
					list.slideDown('fast');
					list.attr("class", "opened");
				} else {
					heading.attr("class", "");
					list.slideUp('fast');
					list.attr("class", "closed");
				}
			});

			var flag = false;

			$("#menu ul li ul li a").click(function() {
				flag = true;
			});

			$("#menu li[class~=collapsible]").click(function() {
				var element = $(this);
				if (!flag) {
					element.children("a:first-child").each(function() {

						var child = $(this);

						if (child.attr("class") == "plus") {
							child.attr("class", "minus");
						} else {
							child.attr("class", "plus");
						}
					});

					element.children("ul").each(function() {
						var child = $(this);

						if (child.attr("class") == "collapsed") {
							child.slideDown('fast');
							child.attr("class", "expanded");
						} else {
							child.slideUp('fast');
							child.attr("class", "collapsed");
						}
					});
					event.preventDefault();
				}
			});
		});