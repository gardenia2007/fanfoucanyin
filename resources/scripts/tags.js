(function($) {
	/*
	 * tags 1.1 copyright (c) 2011 weekface http://weekface.github.com/ date:
	 * 2011-11-15 email: weekface@gmail.com 使用tags可以方便地为您的系统添加很酷的标签添加效果
	 * 
	 * usage:
	 * 
	 * 1, 首先在需要添加的地方放入一个div: <div class="ggtagcontainer"></div>
	 * class必须是ggtagcontainer
	 * 
	 * 并且引入tags.css文件
	 * 
	 * 2, jquery(".ggtagcontainer").tags({ field: "tags1",
	 * //field为保存tags的input(hidden)的name fieldid: "tags1",
	 * //field为保存tags的input(hidden)的id tags: ["北京","上海","南京"], //tags为热门标签 has:
	 * ["美丽","幸福"] //has为已经存在的标签 });
	 * 
	 * 3, 您可以通过jquery.mtags获取添加的标签，或者通过jquery("#tags1")获取
	 */
	$.fn.tags = function(options) {
		var self = $(this), defaults = {
			input : "tag_input",
			field : "tags",
			fieldId : "tags",
			hot_tag : "hot_tag",
			tags : [],
			has : []
		};
		$.mTags = new Array();
		// DONE 增加类别检测，防止用户选择互相矛盾的标签
		$.mTags_c = new Array();

		var options = $.extend(defaults, options), input = options["input"], hot_tag = options["hot_tag"], field = options["field"], fieldId = options["fieldId"], tags = options["tags"], has = options["has"];

		function initial() {
			var tagp = "";
			var tt = "";
			tt += "<div>";
			tt += '<input type="hidden" id="tag_input" />';
			tt += '<input type="hidden" id="tag_class" />';
			tt += '</div>';
			self.append(tt);
			if (field != undefined) {
				self.append('<input type="hidden" id="' + fieldId + '" name="'
						+ field + '" />')
			}
			if (has.length > 0) {
				for ( var i = 0; i < has.length; i++) {
					mDisplay(has[i]);
				}
			}
		}

		initial();

		function mRemove(arr, dx) {
			if (isNaN(dx) || dx > arr.length) {
				return false;
			}
			arr.splice(dx, 1);
		}

		function mInclude(arr, item) {// 检查待添加元素是否已存在
			for ( var i = 0; i < arr.length; i++) {
				if (arr[i] == item) {
					return true
				}
			}
			return false;
		}

		function mAssign() {// 更新hidden输入框中的当前标签
			$("#" + field).val($.mTags.join(','));
			$("#tag_class").val($.mTags.join(','));
			span = self.find("#span");
			if ($.mTags.length == 0) {
				$(".tag-container").hide();
				span.hide();
			} else {
				$(".tag-container").show();
				span.show();
			}
		}

		function mBindClick() { // 绑定删除按钮的点击事件
			self.find("div").find("span").find("a").unbind("click").click(
					function() {
						var a = $(this);
						var t = a.parent().find("span").html();
						var c = a.parent().find("span").attr('id');
						$(this).parent().remove();
						var index = $.mTags.indexOf(t);
						var index_c = $.mTags_c.indexOf(c);
						mRemove($.mTags, index);
						mRemove($.mTags_c, index_c);
						mAssign();
						ajax_load();
						// mFucusInput();
					});
		}

		function mCreateTagSpan(text, tag_class) { // 生成标签的html结构
			var span = '<span deleteable="true" class="tag">';
			span += '<span id=' + tag_class + '>' + text + '</span>';
			span += '<a title="删除" href="javascript:;"> ×</a>';
			span += '</span>';
			return span;
		}

		function mClearInput() {// 清除输入框中的内容
			self.find("div").find("input").val('');
		}

		function mDisplay(text, tag_class) {
			var span = mCreateTagSpan(text, tag_class);
			$.mTags.push(text);
			self.find("div").find("input").val('');
			self.find("div").find("#tag_input").before(span);
			mBindClick();
			mAssign();
			mClearInput();
		}

		function mAddTag(boo) {
			var text = $("#" + input).val();
			var tag_class = $('#tag_class').val();
			// var last = text.split('')[text.length-1];
			// if(boo || last == " " || last == " " || last == "," || last ==
			// "，"){
			if (text == "") {
				return;
			} else if (!(mInclude($.mTags, text))
					&& !(mInclude($.mTags_c, tag_class))) {
				mDisplay(text, tag_class);
				$.mTags_c.push(tag_class);
				ajax_load();
			} else {
				/* 标签重复 */
				return;
			}
			// }
		}

		function ajax_load() {
			// 处理数据
			var tag = $.mTags.join(',');
			var tag_c = $.mTags_c.join(',');
			$("#show-r-load").load("index/search_tag", {
				"tag" : tag,
				"tag_c" : tag_c
			}, function() {
				$("#loading").slideUp(300);
			});

		}
		return this.each(function() {
			$("." + hot_tag).click(function() {
				self.find("div").find("#" + input).val($(this).html());
				self.find("div").find("#tag_class").val($(this).attr('id'));
				mAddTag(false);
			});
		});
	}
})(jQuery);
