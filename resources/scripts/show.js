$(function() {
	$(".cp_other tr")
			.hover(
					function() {
						$(this).addClass("cp_other_hover");
						$(this)
								.children(".cp_show_hidden")
								.css('background',
										'url("../resources/images/add.png") no-repeat scroll 50% 50% transparent');
					},
					function() {
						$(this).removeClass("cp_other_hover");
						if ($(this).children(".cp_show_hidden").attr("id"))
							return;
						else
							$(this).children(".cp_show_hidden").css(
									'background', 'none');
					});

	$(".pic").hover(function() {
		$(this).addClass("cp_img_hover");
		$(this).children(".cp_img_add").show();
	}, function() {
		$(this).removeClass("cp_img_hover");
		if ($(this).children(".cp_img_add").attr('id'))
			return;
		else
			$(this).children(".cp_img_add").hide();
	});
	$("#submit_order").click(function() {
		submit_order_click();
	});
});

var check_order_fill_content = function(){
	
    var total = 0;//订单总价
    var data = $("body").data('jsorder');
	var list = $(".check_order_list");
    list.html("");
    for(var r_id in data){
    	if(data[r_id] === undefined) continue;
    	for (var id in data[r_id]) {

    		console.log(data[r_id][id]);
    		if (data[r_id][id] === undefined || id === "info" || data[r_id][id]['count'] <= 0) continue;
    		list.append(
    				"<li id='dish_" + id + "'>" +
    				"<span class='jsorder_ct_name'>" + data[r_id][id]['ct_name'] + "</span>" +
    				"<span>" + data[r_id][id]['name'] + "" +
    				"<span class='jsorder_order_price'>" + data[r_id][id]['price'] + "元 X " + data[r_id][id]['count'] + data[r_id][id]['unit'] +"</span>" +
    				"</span>" +
    				"</li>"
    		);
    		total += parseFloat(data[r_id][id]['count']) * parseFloat(data[r_id][id]['price']);
    		//console.log(total);
    		//total2 += parseFloat(parseFloat(data[id]['count']) * parseFloat(data[id]['price'])).toFixed(2);
    	}
    	list.append(
				"<li id='delivery_" + r_id + "'>" +
				"<span class='jsorder_ct_name'>" + data[r_id]['info']['ct_name'] + "</span>" +
				"<span>配送费" +
				"<span class='jsorder_order_price'>" + data[r_id]['info']['price'] + "元</span>" +
				"</span>" +
				"</li>"
		);
        total += parseFloat(data[r_id]['info']['price']);
		//console.log(total);
    }
    $(".check_order_total").html("<span>订单总计 "+total+" 元<span>");
    
};

var check_order = function() {

	check_order_fill_content();
    
    $("#check_order_dialog").dialog({
       width:400,
       modal:true,
       title:"请核对订单",
       buttons:{
       			"确定，下一步":submit_order_click,
   				"返回修改订单" : function() {
    				$(this).dialog("close");
       			}
    		}
    });
};


var submit_order_click = function() {
	$(this).dialog("close");
	var postdata = $("body").data('jsorder');
	// opts.dosubmit(postdata, opts.staticname);

	$("#dialog_form").dialog( {
		height : 360,
		width : 450,
		modal : true,
		title : "请输入送餐信息",
		buttons : {
			"确定信息，提交订单" : button_yes,
			"取消" : function() {
				$(this).dialog("close");
			}
		},
		open : fill_info
	});
};

var button_yes = function() {
	var postdata = $("body").data('jsorder');
	var address = $("#address"), 
	building = $("#building"), 
	room = $("#room"), 
	phone = $("#phone"), 
	allFields = $([]).add(address).add(building).add(room).add(phone);
	allFields.removeClass("ui-state-highlight");

	var bValid = true;

	bValid = bValid & checkLength(building, "楼宇名称", 0, 30);

	if (bValid)
		bValid = bValid & checkLength(room, "房间号", 0, 30);
	if (bValid)
		bValid = bValid & checkLength(phone, "联系电话", 8, 11);// 联系电话至少为8位（固定电话），最多11位（移动电话）
	if (bValid)
		bValid = bValid & checkRegexp(phone, /^([0-9])+$/, "电话号码只能数字！");

	// bValid = bValid & checkRegexp(level,/^([0-9][a-z][A-Z])+$/,
	// "房间号只能是数字和字母！");
	// bValid = bValid & checkLength(company,"公司名称",0,30);

	if (bValid) { // 若表单数据合法
		postdata['info'] = {};
		postdata['info']['address'] = address.val();
		postdata['info']['building'] = building.val();
		postdata['info']['room'] = room.val();
		postdata['info']['phone'] = phone.val();
		// alert(postdata['info']);
		$(this).dialog("close");
		// ajax提交数据
		$.post(url + "order/submit",// 提交地址
				postdata, success_callback);

        var date = new Date();
        date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
        $.cookie('fanfou_rem', JSON.stringify(postdata['info']), {
            path: '/',
            expires: date
          });
	}
};

//输入框赋值
var fill_info = function() {
	data = eval('(' + $.cookie("fanfou_rem") + ')');
	//console.log(data);
	$("#address").val(data.address);
	$("#building").val(data.building);
	$("#room").val(data.room);
	$("#phone").val(data.phone);
};

var success_callback = function(data) {
	/******************************************************************************** */
	if (data == '0') { // 提交成功
		$("#loading").html("<img src='"+url+"resources/images/success.png'/><p>订单提交成功！正在为您跳转...</p>");
		setTimeout(function() {
			$("#loading").hide();
			location.href = url + "order/show"; // 跳转到我的订单页面
			}, 1200);
		// 提交成功，删除购物车中的内容
		empty_jsorder();
	} else { // 提交失败
		alert(data);
		$("#loading").html("<img src='" + url + "resources/images/wrong.png'/><p>订单提交失败，请重试！</p>");
		setTimeout(function() {
			$("#loading").hide();
		}, 1200);
	}
};

function empty_jsorder(){
	$("body").data('jsorder', {});
	console.log($("body").data('jsorder'));
	var date = new Date();
	date.setTime(date.getTime() - (3600));
	$.cookie('jsorder', '',{ 
		path: '/',
		expires: date
	});
};
