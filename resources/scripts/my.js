
//Function函数

function login(url){
	$("#dialog").dialog({//对话框
		height:240,
		width:350,
		modal:true,
		title:"登录提示",
		buttons:{
			"知道了":function(){
				$(this).dialog("close");
			}
		},
		open:function(event, ui){
			$(this).html("").append("<strong >请登录后使用本应用！</strong><a href='"+url+"' style='display: block;margin-top: 25px;margin-left: 40px;'><img src='images/weibo_login.png'/></a>");
		}
	});
}

function my_error(content, id){
	
	$("#dialog").dialog({
		height:200,
		modal:true,
		title:"提示",
		//hide:"slide",
		buttons:{
			"知道了":function(event, ui){
				$(this).dialog("close");
				//TODO 关闭对话框后焦点移到输入框
				//var id = "#" + id;
				//$(id).show().trigger("focus").next().show();
			}
		},
		open:function(event, ui){
			$(this).html("").append("<p>" + content + "</p>");
		}
	});

}

function updateTips( t ) {
	var tips = $("#dialog_form_tips");
	tips.html( t ).addClass( "ui-state-highlight" );
	//settimeout(function() {tips.removeclass( "ui-state-error", 1500 );}, 500 );
}

function checkRegexp( o, regexp, n ) {
	if(!(regexp.test(o.val()))) {
		o.addClass( "ui-state-highlight" );
		updateTips( n );
		return false;
	} else {
		return true;
	}
}

function checkLength( o, n, min, max ) {
	if(o.val().length == 0){
		o.addClass( "ui-state-highlight" );
		updateTips(  n + "不可为空！" );
		return false;
	}else if(o.val().length < min){
		o.addClass( "ui-state-highlight" );
		updateTips(  n + "位数不可少于"+min+"位，您输入了"+o.val().length+"位" );
		return false;
	}else if(o.val().length > max){
		o.addClass( "ui-state-highlight" );
		updateTips(  n + "位数不可多于" + max + "位，您输入了"+o.val().length+"位" );
		return false;
	} else {
		return true;
	}
}



