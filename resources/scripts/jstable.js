(function($) {
	$.fn.jstable = function() {
		var element = $(this).children("table tbody");

		$(element).children("tr > td").hover(function() {
			$(this).toggleClass("tr_hover");
		});

		// .addClass("jstable_evev_a")
		var i = 1;
		$(element).children("tr").each(function() {
			if (i % 2 == 1) {
				$(this).hover(function() {
					$(this).children("td").toggleClass("tr_hover");
				});
			}
			if (i % 4 == 3)
				$(this).children("td").addClass("jstable_a");
			i++;
		});

		$(element).children("tr:even").addClass("jstable_even");
		$(element).children("tr:not(.jstable_even)").hide().addClass(
				"jstable_tr");
		// $(element).children("tr:first-child").show().removeClass("jstable_tr");
		// $(".show_in_show tr").show();

		$(element).find("tr.jstable_even .show a").click(function(e) {
			$(this).parent().parent().next("tr").toggle();
			e.preventDefault();
		});
	};
})(jQuery);