(function($) {
  $.fn.jsorder = function(setting) {
    //初始化配置
    var opts = $.extend({},
    $.fn.jsorder.defaults, setting);
    //读取cookeie信息
    var initdata = {};
    var flag = false;//是否已存在购物车
    if (opts.savecookie && $.cookie(opts.cookiename) != null && $.cookie(opts.cookiename) != '') {
    	flag = true;
      initdata = eval('(' + $.cookie(opts.cookiename) + ')');
    }

    //初始化购物车
    $("body").data(opts.staticname, initdata);
    //初始化页面

    if(flag || $("#jsorder").length != 0){
    	$("#jsorder").remove();
    }
    var jsorder = $(
    		'<div id="jsorder"><h3 >' + opts.leftdemo 
    		+'</h3><a id="clear-order" style="position:absolute;top:7px;right:20px;cursor:pointer">清空订单</a>'
    		+'<div><div><ul><li style="text-align:center">' + opts.nomessage
    		+'</li></ul></div></div></div>'
    		).attr('class', opts.jsorderclass).appendTo($("#my-order"));

    var jsorderright = jsorder.find('div:eq(1)').attr('class', 'right');

    //var jsorderleft = jsorder.find('div:eq(0)').attr('class', 'top').click(function() {jsorderright.toggle();});
    var slide = {
    		
    //新建一个li	
    create_li:function(r_id, id, name, price, count, ct_name, unit){
    	
        var info_data = {r_id:r_id,id:id,name:name,price:price};
        $("<li id='" + opts.jsorderpre + id + "'>" +
          	  "<span class='jsorder_ct_name'>"  + ct_name + "</span>" +
          	  "<span>" +
          	  	"<span>" + name +"</span>" +
          	  	"<span class='jsorder_order_price'>" + price + "元 X " + count + unit + "</span>" +
          	  	"<span  class='jsorder_del'></span>" +
          	  	"<span  class='jsorder_sub'></span>" +
          	  	"<span class='jsorder_add'></span>" +
          	  "</span></li>")
          	  .appendTo($("div." + opts.jsorderclass + " ul"))
          	  .data("info",info_data);
            //$("div." + opts.jsorderclass + " ul").append(content);
    },

      //新增一个订单项
    addjsorder: function(e) {
        var datajsorder = $("body").data(opts.staticname);
        console.log(datajsorder);
        var id = $(this).attr('id');
        var name = $(this).attr(opts.namefiled);
        var price = $(this).attr(opts.pricefiled);
        var unit = $(this).attr("cp_unit");
        var r_id = $("#r_show_info").attr("r_id");
        var ct_name = $("#r_name").html();
        var deliveryprice = $("#r_delivery_price span").html();

        if(datajsorder[r_id] == null){
        	datajsorder[r_id] = {};
        	datajsorder[r_id]['info'] = {};
        	datajsorder[r_id]['info']['ct_name'] = ct_name;
        	datajsorder[r_id]['info']['price'] = deliveryprice;
        }

        if (datajsorder[r_id][id] == null || datajsorder[r_id][id]['count'] == 0) {
        	if (datajsorder[r_id][id] == null) {
        		datajsorder[r_id][id] = {};
        	}
        	datajsorder[r_id][id]['count'] = 1;
        	datajsorder[r_id][id]['name'] = name;
        	datajsorder[r_id][id]['price'] = price;
        	datajsorder[r_id][id]['r_id'] = r_id;
        	datajsorder[r_id][id]['ct_name'] = ct_name;
        	datajsorder[r_id][id]['unit'] = unit;
        	
        	slide.create_li(r_id, id, name, price, 1, ct_name, unit);
        	slide.show_img(id);
        	slide.bindclick(id);

        } else {
        	datajsorder[r_id][id]['count'] += 1;
        	$("#" + opts.jsorderpre + id + " .jsorder_order_price")
        	.html(datajsorder[r_id][id]['price'] + "元 X " + datajsorder[r_id][id]['count'] + "份");
        }
       	slide.reflash();
      },
      //增加一个订单项数量
      addjsordernum: function(r_id, id) {
    	  var datajsorder = $("body").data(opts.staticname);
    	  datajsorder[r_id][id]['count']++;
    	  $("#" + opts.jsorderpre + id + " .jsorder_order_price").html(datajsorder[r_id][id]['price'] + "元 X " + datajsorder[r_id][id]['count'] + "份");
    	  slide.reflash();
      },
      //减少一个订单项数量
      deljsordernum: function(r_id, id) {
    	  var datajsorder = $("body").data(opts.staticname);
		  datajsorder[r_id][id]['count']--;
//    	  if(datajsorder[r_id][id]['count'] >= 1)
//    	  else datajsorder[r_id][id]['count']=0;
    	  if (datajsorder[r_id][id]['count'] > 0) {
    		  $("#" + opts.jsorderpre + id + " .jsorder_order_price").html(datajsorder[r_id][id]['price'] + "元 X " + datajsorder[r_id][id]['count'] + "份");
    	  } else {
    		  datajsorder[r_id][id] = undefined;
    		  $("#" + opts.jsorderpre + id).remove();
    		  slide.hide_img(id);
    	  }
    	  slide.reflash();
      },
      //删除一个订单项
      deljsorder: function(r_id, id) {
    	  var datajsorder = $("body").data(opts.staticname);
    	  datajsorder[r_id][id] = undefined;
    	  $("#" + opts.jsorderpre + id).remove();
    	  slide.hide_img(id);
    	  slide.reflash();
      },
      subm: check_order,
      //提交
      empty: function(){
    	  //去除菜品上的图标
    	  for (var r_id in data) {
    		  for(var id in data[r_id]){
    			  slide.hide_img(id);
    		  }
    	  }
    	  $("body").data(opts.staticname, {});
    	  $("div." + opts.jsorderclass + " ul li:eq(0)").show();
    	  $("div." + opts.jsorderclass + " ul li:gt(0)").remove();
    	  $('div.' + opts.jsorderclass + ' a.button').remove();
    	  if (opts.savecookie) {
    		  var date = new Date();
    		  date.setTime(date.getTime() - (3600));
    		  $.cookie(opts.cookiename, '', {
    			  path: '/',
    			  expires: date
    		  });
    	   }
	   },
	   show_img: function(id){
		   $("#" + id + " .cp_img_add").show().attr("id", "ciaf");
		   $("#" + id + " .cp_show_hidden").css("background","url('../resources/images/add.png') no-repeat scroll 50% 50% transparent").attr("id", "ciaf");
	   },
	   hide_img: function(id){
	       $("#" + id + " .cp_img_add").hide().attr("id", "");
	       $("#" + id + " .cp_show_hidden").css('background', 'none').attr("id", "");
	   },
	   
	   //为添加，减少，删除按钮绑定事件
	   bindclick:function(id){
		   $("#" + opts.jsorderpre + id + " span.jsorder_add").click(function() {
			   var d = $(this).parent().parent().data("info");
			   slide.addjsordernum(d.r_id, d.id);
			   });
		   $("#" + opts.jsorderpre + id + " span.jsorder_sub").click(function() {
			   var d = $(this).parent().parent().data("info");
			   slide.deljsordernum(d.r_id, d.id);
			   });
		   $("#" + opts.jsorderpre + id + " span.jsorder_del").click(function() {
			   var d = $(this).parent().parent().data("info");
			   slide.deljsorder(d.r_id, d.id);
			   });
	   },
      //刷新购物车
      reflash: function() {
        jsorderright.show();
        var data = $("body").data(opts.staticname);
        var flag = false;
        // 
        for (var i in data) {
        	for(var j in data[i]){
        		if(data[i][j] === undefined || j === "info") continue;
        		if (data[i][j]['count'] != 0)flag = true;
        		else data[i][j] = undefined;
        	}
        	//if(flag) break;
        }
        
        var f = false;//是否有菜品
        for (var i in data){
        	//console.log(data[i]);
        	for(var j in data[i]){
        		if(j !== "info"){
        			f = true;
        			break;
        		}
        	}
        	if(!f)//如果该餐馆没有菜品了，就删除这个餐馆
        		data[i] = undefined;
        	
    		f = false;
        	//console.log(data[i]);
        }
        if (flag) {
        	$("div." + opts.jsorderclass + " ul li:eq(0)").hide();
        	if ($('div.' + opts.jsorderclass + ' a.button').size() == 0) $('<a class="button">' + opts.subbutton + '</a>').appendTo(jsorderright).click(slide.subm);
        } else {
        	$("div." + opts.jsorderclass + " ul li:eq(0)").show();
        	$('div.' + opts.jsorderclass + ' a.button').remove();
        }
        
        if (opts.savecookie) {
          var date = new Date();
          date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
          $.cookie(opts.cookiename, JSON.stringify(data), {
              path: '/',
              expires: date
            });
        }
      }
    };
    //初始化购物车
    var data = $("body").data(opts.staticname);
    //遍历
    for (var r_id in data) {
    	for(var id in data[r_id]){
    		if(data[r_id][id] === undefined || id === "info") continue;

    		if (data[r_id][id]['count'] <= 0) {slide.deljsorder(r_id, id);continue;}
    		slide.create_li(r_id, id, data[r_id][id]['name'], data[r_id][id]['price'], data[r_id][id]['count'], data[r_id][id]['ct_name'], data[r_id][id]['unit']);
    		slide.show_img(id);
    		slide.bindclick(id);
    		slide.reflash();
    	}
    }
    
    $(opts.addbutton).click(slide.addjsorder);
    //清空订单
	$("#clear-order").click(function(){
		slide.empty();
	});
    return jsorder;
  };
  // 配置
  $.fn.jsorder.defaults = {
    //全局数据-保存订单信息
    staticname: 'jsorder',
    //订单class
    jsorderclass: 'jsorder',
    //是否保存cookie
    savecookie: true,
    //cookie的名字
    cookiename: 'jsorder',
    //ID前缀
    numpre: 'no_',
    //订单项前最
    jsorderpre: 'fanfou_',
    //价格属性
    pricefiled: 'cp_price',
    //订单项名
    namefiled: 'cp_name',
    //单位属性
    unitfiled: 'cp_unit',
    //购物车左侧显示
    leftdemo: '我的订单',
    //提交按钮文字
    subbutton: '',
    //默认加入订单的控件选择
    addbutton: '.addcp',
    //没有订单时显示
    nomessage: '您还没有添加任何菜品！',
    //提交时触发
    dosubmit: function(data) {},
    //url
    url: ''
  };
})(jQuery);
