function autoScroll(obj) {
	$(obj).find("ul:first").animate( {
		marginTop : "-20px"
	}, 500, function() {
		$(this).css( {
			marginTop : "0px"
		}).find("li:first").appendTo(this);
	});
}

$(function() {

	setInterval('autoScroll("#news")', 2000);
})
